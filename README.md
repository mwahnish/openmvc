# README #

### Version 0.5 ###

#### Warning: This framework is a working system, but is still in the early stages of development. Do not use this code in a production environment, the API will change often, leaving you with broken code. You have been warned!!####

### What is OpenMVC ###

OpenMVC is an open source framework built on top Unity's Monobehaviour class that enforces a Model-View-Framework (MVC) software design pattern onto your components. MVC is a very common and simple design pattern used to separate presentation and control from your software model. Ultimately, MVC is a tool for writing better organized and data-driven code.

An OpenMVC component is made up of 4 parts:

**Model**

The Model is a class containing the data for your component. It receives no callbacks from Unity (message functions like Update or Start). All references to other components, like Transforms or RigidBodies, go through this class. It also provides a system for other classes to register callbacks that will be called when data in the model changes.

**View**

The View is the presentation layer of your component. It is a class containing code for displaying the data in the scene. It also provides access to Unity callbacks related to rendering (OnRender, OnGUI, etc.)

**Controller**

The Controller is responsible for operating on the model. It is the layer between the player / other classes and the model. The controller takes input, filters it, and modifies the model. It receives Unity callbacks like Update, and OnStart.

**MVCMonobehaviour**
The MVCMonobehaviour class is the wrapper for the Model, View, and Controller classes, and provides interoperability between the classes. It is also what allows the Model, View, and Controller classes to be added as a single component to a GameObject.

### How do I get set up? ###

Setting up OpenMVC is easy! Either download and unzip the repository into your assets folder, or clone the repo using Git. That's it!

### Using OpenMVC ###
Docs coming soon!

### Contribution guidelines ###

Contributions are very welcome! Feel free to clone and submit pull requests as you please!

### Contact ###

[mark@abxygames.com](mailto://mark@abxygames.com)

[@mark_at_abxy](twitter.com/mark_at_abxy)

### License ###
OpenMVC is released under [The MIT License](https://bitbucket.org/mwahnish/openmvc/src/13b0c8f5c930b7b8659b42a5c047e3765f4edf81/License?at=master)