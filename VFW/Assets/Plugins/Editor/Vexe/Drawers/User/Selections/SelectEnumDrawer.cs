using System;
using VexeEmbedded.Editor.Windows;
using VexeEmbedded.Runtime.Extensions;
using VexeEmbedded.Runtime.Types;

namespace VexeEmbedded.Editor.Drawers
{
	public class SelectEnumDrawer : CompositeDrawer<Enum, SelectEnumAttribute>
	{
		public override void OnRightGUI()
		{
			if (gui.SelectionButton())
			{
				string[] names = Enum.GetNames(memberType);
				int currentIndex = memberValue == null ? -1 : names.IndexOf(memberValue.ToString());
				SelectionWindow.Show(new Tab<string>(
					@getValues: () => names,
					@getCurrent: () => memberValue.ToString(),
					@setTarget: name =>
					{
						if (names[currentIndex] != name)
                        { 
							memberValue = name.ParseEnum(memberType);
                            member.Write();
                        }
					},
					@getValueName: name => name,
					@title: memberTypeName + "s"
				));
			}
		}
	}
}
