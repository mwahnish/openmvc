﻿using VexeEmbedded.Editor.Helpers;
using VexeEmbedded.Runtime.Extensions;
using VexeEmbedded.Runtime.Types;

namespace VexeEmbedded.Editor.Drawers
{
	public class InputAxisDrawer : AttributeDrawer<string, InputAxisAttribute>
	{
		private string[] axes;
		private int current;

		protected override void Initialize()
		{
			if (memberValue == null)
				memberValue = "";

			axes = EditorHelper.GetInputAxes().ToArray();
			current = axes.IndexOfZeroIfNotFound(memberValue);
		}

		public override void OnGUI()
		{
			var x = gui.Popup(displayText, current, axes);
			{
				var newValue = axes[x];
				if (current != x || memberValue != newValue)
				{
					memberValue = newValue;
					current = x;
				}
			}
		}
	}
}