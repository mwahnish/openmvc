﻿using UnityEngine;
using VexeEmbedded.Editor.Helpers;
using VexeEmbedded.Runtime.Types;
using UnityObject = UnityEngine.Object;

namespace VexeEmbedded.Editor.Drawers
{
	public class DraggableDrawer : CompositeDrawer<UnityObject, DraggableAttribute>
	{
		public override void OnMemberDrawn(Rect rect)
		{
			gui.RegisterFieldForDrag(rect, memberValue);
		}
	}
}