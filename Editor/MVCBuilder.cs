﻿using UnityEngine;
using System.Collections;
using UnityEditor;
using System.IO;

namespace ABXY.OpenMVC.Editor {
    /// <summary>
    /// This class provides a convenient right-click menu helper for creating MVC systems
    /// </summary>
    public class MVCBuilder : EditorWindow {

        /// <summary>Base name for the classes</summary>
        public static string className = "MyMVC";

        public static string modelName = "MyMVC_M";

        public static string viewName = "MyMVC_V";

        public static string controllerName = "MyMVC_C";

        /// <summary>Where to generate the files</summary>
        public static string path = "";

        #region class stubs
        /// <summary>Stub for a generic monobehaviour</summary>
        private static string monobehaviourStub = @"using UnityEngine;
using System.Collections;
using ABXY.OpenMVC;

public class <ClassName> :  MVCMonobehaviour<<ModelType>,<ViewType>,<ControllerType>>{

    
}

";

        /// <summary>Stub for a generic model</summary>
        private static string modelStub = @"public class <ClassName>: Model {
 //Caching and exposing needed components
    private Transform transform_p;
    public Transform transform {
        get {
            if (transform_p == null) {
                transform_p = (Transform)GetComponent(typeof(Transform));
            }
            return transform_p;
        }
    }
}

";

        /// <summary>Stub for a generic view</summary>
        private static string viewStub = @"public class <ClassName> : View<<ControllerType>,<ModelType>>{
//Unity Callbacks
    private void OnGUI(){
    
    }
}

";

        /// <summary>Stub for a generic controller</summary>
        private static string controllerStub = @"public class <ClassName> : Controller<<ModelType>>{
    //Unity Callbacks
    private void Update(){
    
    }
}

";
        #endregion

        /// <summary>
        /// Right clic menu option located at 'Assets/Create/Generate MVC'. Starts the generation of the classes
        /// </summary>
        [MenuItem("Assets/Create/Generate MVC")]
        static void MVCGen() {

            /*The following code is from 
             * http://forum.unity3d.com/threads/how-to-get-currently-selected-folder-for-putting-new-asset-into.81359/.
             * Thanks yoyo */

            string path = "Assets";
            foreach (UnityEngine.Object obj in Selection.GetFiltered(typeof(UnityEngine.Object), SelectionMode.Assets)) {
                path = AssetDatabase.GetAssetPath(obj);
                if (File.Exists(path)) {
                    path = Path.GetDirectoryName(path);
                }
                break;
            }
            MVCBuilder.path = Application.dataPath + path.Substring(6);
            MVCBuilder window = (MVCBuilder)EditorWindow.GetWindow(typeof(MVCBuilder));
            window.Show();
            //window.position = new Rect(window.position.x, window.position.y, 400f, 200f); 
        }

        /// <summary>
        /// Displays the editorwindow. Provides options for generating the code.
        /// </summary>
        void OnGUI() {

            // naming the next text field so I can set focus
            GUI.SetNextControlName("ClassNameField");

            // Generating each stub
            className = EditorGUILayout.TextField("Class Name", className);
            modelName = EditorGUILayout.TextField("Generate Model", modelName);
            viewName = EditorGUILayout.TextField("Generate View", viewName);
            controllerName = EditorGUILayout.TextField("Generate Controller", controllerName);

            // Focusing the text field
            //GUI.FocusControl("ClassNameField");

            // Watching for the enter key
            bool keyboardMake = (Event.current != null) && (Event.current.keyCode == KeyCode.Return);

            // Generating with the specified options
            if (GUILayout.Button("Make") || keyboardMake) {
                string classContent = GenerateClass(monobehaviourStub, className);
                classContent = classContent + GenerateClass(modelStub, modelName);
                classContent = classContent + GenerateClass(viewStub, viewName);
                classContent = classContent + GenerateClass(controllerStub, controllerName);
                GenerateFile(GenerateClass(classContent, className), path, className + ".cs");
                AssetDatabase.Refresh();
                this.Close();
            }

        }

        /// <summary>
        /// Generic method for creating a class
        /// </summary>
        /// <param name="stub">Specify a model, view, or controller stub</param>
        /// <param name="className">The name of the class you intend to make</param>
        /// <returns>String containing a class stub that has been filled out appropriately</returns>
        private static string GenerateClass(string stub, string className) {
            string text = stub;
            text = text.Replace("<ClassName>", className);


            //Setup Model
            text = text.Replace("<ModelType>", modelName);


            //Setup View

            text = text.Replace("<ViewType>", viewName);



            //Setup Controller

            text = text.Replace("<ControllerType>", controllerName);

            return text;
        }


        /// <summary>
        /// This method writes the class file
        /// </summary>
        /// <param name="contents">Text to write into the file</param>
        /// <param name="location">Where to write the file</param>
        /// <param name="name">Name of the file, including extension</param>
        private static void GenerateFile(string contents, string location, string name) {
            if (location == "")
                location = Application.dataPath;
            StreamWriter sw = new StreamWriter(location + "/" + name);
            sw.Write(contents);
            sw.Close();
        }
    }
}