﻿using UnityEngine;
using UnityEditor;
using ABXY.OpenMVC.Helpers.StateMachine;

namespace ABXY.OpenMVC.Editor.StateMachineUI {

    [CustomPropertyDrawer(typeof(State))]
    public class StateDrawer : PropertyDrawer {
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label) {
            EditorGUI.BeginProperty(position, label, property);
            Rect propertyRect = new Rect(position.x, position.y, position.width, position.height);
            EditorGUI.PropertyField(propertyRect, property.FindPropertyRelative("stateName"), GUIContent.none);
            property.serializedObject.ApplyModifiedProperties();
            EditorGUI.EndProperty();
        }
    }
}