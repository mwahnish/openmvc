﻿using UnityEngine;
using UnityEditor;
using ABXY.OpenMVC.Helpers.StateMachine;

namespace ABXY.OpenMVC.Editor.StateMachineUI{ 

    [CustomPropertyDrawer(typeof(StateMachine))]
    public class StateMachineDrawer : PropertyDrawer {
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label) {
            EditorGUI.BeginProperty(position, label, property);
            EditorGUI.indentLevel++;
            SerializedProperty states = property.FindPropertyRelative("states");

            Rect labelSize = new Rect(0, position.y, 200f, 20f);
            EditorGUI.PropertyField(labelSize, states, label);

            //if (property.isExpanded) {
            GUI.BeginGroup(position);
            for (int index = 0; index < states.arraySize; index++) {
                Rect propertyRect = new Rect(0, (index * 20) + 20, position.width / 2f, 20);
                Rect deleteRect = new Rect(position.width / 2f, (index * 20) + 20, position.width / 2f, 20);
                EditorGUI.PropertyField(propertyRect, states.GetArrayElementAtIndex(index));
                if (GUI.Button(deleteRect, "Delete")) {
                    states.DeleteArrayElementAtIndex(index);
                }
            }
            Rect addButtonRect = new Rect(0, (states.arraySize * 20) + 20, position.width, 20);
            if (GUI.Button(addButtonRect, "Add New State")) {
                states.InsertArrayElementAtIndex(states.arraySize);
            }
            GUI.EndGroup();
            //}

            property.serializedObject.ApplyModifiedProperties();
            EditorGUI.indentLevel--;
            EditorGUI.EndProperty();


        }

        public override float GetPropertyHeight(SerializedProperty property, GUIContent label) {
            SerializedProperty states = property.FindPropertyRelative("states");
            return (states.arraySize * 20) + 40;
        }
    }
}
