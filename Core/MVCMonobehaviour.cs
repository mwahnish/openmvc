﻿// Copyright (c) 2015 ABXY Games, released under The MIT License
using UnityEngine;
using System.Collections.Generic;
using ABXY.OpenMVC.Helpers;
using ABXY.OpenMVC.Helpers.DelegateSystem;
using ABXY.OpenMVC.Helpers.ChangeSubscription;
using ABXY.OpenMVC.Helpers.Methods;
using VexeEmbedded.Runtime.Types;

namespace ABXY.OpenMVC {
    /// <summary>
    /// Extend this class instead of Monobehaviour, in order to use MVC. A Model,
    /// View, and Controller class must be created for each MVCMonobehaviour, and the types
    /// of each are the type parameters for this class.
    /// </summary>
    /// <typeparam name="modelType"></typeparam>
    /// <typeparam name="viewType"></typeparam>
    /// <typeparam name="ControllerType"></typeparam>
    public class MVCMonobehaviour <modelType,viewType,ControllerType> : GenericMVCMonobehaviour
        where modelType : Model, new()
        where viewType : View<ControllerType,modelType>, new()
        where ControllerType : Controller<modelType>, new() {
       
        /// <summary>
        /// The model instance for this MVCMonobehaviour
        /// </summary>
        [Serialize]
        protected modelType model = new modelType();

        /// <summary>
        /// The view instance for this MVCMonobehaviour
        /// </summary>
        [Serialize]
        protected viewType view = new viewType();

        /// <summary>
        /// The controller instance for this MVCMonobehaviour
        /// </summary>
        [Serialize]
        protected ControllerType controller = new ControllerType();

        /// <summary>
        /// Delegate container - used to hold the callback delegate references
        /// for the MVC monobehaviour
        /// </summary>
        DelegateContainer dc;

        /// <summary>
        /// This is an entry point for a change subscription system allowing
        /// other class to register callbacks to watch variables for changes
        /// </summary>
        [HideAttribute]
        ChangeSubscriptionHandler changeSubscription = new ChangeSubscriptionHandler();

        private MethodList<modelType,viewType,ControllerType> methodList;
        

            //Code responsible for setting up MVC
        #region Setup Functions



        /// <summary>
        /// Calls everything needed to set up MVC
        /// </summary>
        private void MakeReady() {
            
                if (model != null)
                    model.Initialize(this);
                if (view != null)
                    view.initialize(this, model, controller);
                if (controller != null)
                    controller.Initialize(this, model);

                // Initializing the methodList
                methodList = new MethodList<modelType,viewType,ControllerType>(model, view, controller);
                dc = new DelegateContainer();

                //grabbing the class's members

                model.changeSubscription.RemoveSubscribers();
                
            
        }

        


        /*
        /// <summary>
        /// Method to retrieve methods as delegates from given class;
        /// </summary>
        private void SetupControlDelegates() {
            
            MemberInfo[] members = controller.GetType().GetMembers(BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.FlattenHierarchy);
            foreach (MemberInfo member in members) {
                if (member.MemberType == MemberTypes.Method) {
                    DelegateDefinitions.events eventType = DelegateDefinitions.IsADelegate(DelegateDefinitions.MemberOf.control, member.Name);
                    HookUpDelegate(eventType, controller, (MethodInfo)member);
                    
                    
                }
            }
        }*/
        /*
        /// <summary>
        /// Method to retrieve methods as delegates from given class;
        /// </summary>
        private void SetupViewDelegates() {

            MemberInfo[] members = view.GetType().GetMembers(BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.FlattenHierarchy);
            foreach (MemberInfo member in members) {
                if (member.MemberType == MemberTypes.Method) {
                    DelegateDefinitions.events eventType = DelegateDefinitions.IsADelegate(DelegateDefinitions.MemberOf.view, member.Name);
                    HookUpDelegate(eventType, view, (MethodInfo)member);


                }
            }
        }

        /// <summary>
        /// Method to retrieve methods as delegates from given class;
        /// </summary>
        private void SetupModelDelegates() {

            MemberInfo[] members = model.GetType().GetMembers(BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.FlattenHierarchy);
            foreach (MemberInfo member in members) {
                if (member.MemberType == MemberTypes.Method) {
                    DelegateDefinitions.events eventType = DelegateDefinitions.IsADelegate(DelegateDefinitions.MemberOf.model, member.Name);
                    HookUpDelegate(eventType, model, (MethodInfo)member);


                }
            }
        }*/

        

        //Takes a given event type and method, generates a delegate, and attaches it.
        /*private void HookUpDelegate(DelegateDefinitions.events eventType, object targetObject, MethodInfo method) {
            switch (eventType) {
                case DelegateDefinitions.events.Update:
                    //dc.updateDel -= (DelegateDefinitions.UpdateType)System.Delegate.CreateDelegate(typeof(DelegateDefinitions.UpdateType), targetObject, method);
                    //dc.updateDel += (DelegateDefinitions.UpdateType)System.Delegate.CreateDelegate(typeof(DelegateDefinitions.UpdateType), targetObject, method);
                    dc.updateDel.Assign( method);
                    break;
                case DelegateDefinitions.events.Start:
                    //dc.startDel -= (DelegateDefinitions.StartType)System.Delegate.CreateDelegate(typeof(DelegateDefinitions.StartType), targetObject, method);
                    //dc.startDel += (DelegateDefinitions.StartType)System.Delegate.CreateDelegate(typeof(DelegateDefinitions.StartType), targetObject, method);
                    dc.startDel.Assign(method);
                    break;
                case DelegateDefinitions.events.Reset:
                    //dc.resetDel -= (DelegateDefinitions.ResetType)System.Delegate.CreateDelegate(typeof(DelegateDefinitions.ResetType), targetObject, method);
                    //dc.resetDel += (DelegateDefinitions.ResetType)System.Delegate.CreateDelegate(typeof(DelegateDefinitions.ResetType), targetObject, method);
                    dc.resetDel.Assign(method);
                    break;
                case DelegateDefinitions.events.OnValidate:
                    //dc.onValidateDel -= (DelegateDefinitions.OnValidateType)System.Delegate.CreateDelegate(typeof(DelegateDefinitions.OnValidateType), targetObject, method);
                    //dc.onValidateDel += (DelegateDefinitions.OnValidateType)System.Delegate.CreateDelegate(typeof(DelegateDefinitions.OnValidateType), targetObject, method);
                    dc.onValidateDel.Assign(method);
                    break;
                case DelegateDefinitions.events.OnTriggerStay2D:
                    //dc.onTriggerStay2DDel -= (DelegateDefinitions.OnTriggerStay2DType)System.Delegate.CreateDelegate(typeof(DelegateDefinitions.OnTriggerStay2DType), targetObject, method);
                    //dc.onTriggerStay2DDel += (DelegateDefinitions.OnTriggerStay2DType)System.Delegate.CreateDelegate(typeof(DelegateDefinitions.OnTriggerStay2DType), targetObject, method);
                    dc.onTriggerStay2DDel.Assign(method);
                    break;
                case DelegateDefinitions.events.OnTriggerStay:
                    //dc.onTriggerStayDel -= (DelegateDefinitions.OnTriggerStayType)System.Delegate.CreateDelegate(typeof(DelegateDefinitions.OnTriggerStayType), targetObject, method);
                    //dc.onTriggerStayDel += (DelegateDefinitions.OnTriggerStayType)System.Delegate.CreateDelegate(typeof(DelegateDefinitions.OnTriggerStayType), targetObject, method);
                    dc.onTriggerStayDel.Assign(method);
                    break;
                case DelegateDefinitions.events.OnTriggerExit2D:
                    //dc.onTriggerExit2DDel -= (DelegateDefinitions.OnTriggerExit2DType)System.Delegate.CreateDelegate(typeof(DelegateDefinitions.OnTriggerExit2DType), targetObject, method);
                    //dc.onTriggerExit2DDel += (DelegateDefinitions.OnTriggerExit2DType)System.Delegate.CreateDelegate(typeof(DelegateDefinitions.OnTriggerExit2DType), targetObject, method);
                    dc.onTriggerExit2D.Assign(method);
                    break;
                case DelegateDefinitions.events.OnTriggerExit:
                    //dc.onTriggerExitDel -= (DelegateDefinitions.OnTriggerExitType)System.Delegate.CreateDelegate(typeof(DelegateDefinitions.OnTriggerExitType), targetObject, method);
                    //dc.onTriggerExitDel += (DelegateDefinitions.OnTriggerExitType)System.Delegate.CreateDelegate(typeof(DelegateDefinitions.OnTriggerExitType), targetObject, method);
                    dc.onTriggerExitDel.Assign(method);
                    break;
                case DelegateDefinitions.events.OnTriggerEnter2D:
                    //dc.onTriggerEnter2DDel -= (DelegateDefinitions.OnTriggerEnter2DType)System.Delegate.CreateDelegate(typeof(DelegateDefinitions.OnTriggerEnter2DType), targetObject, method);
                    //dc.onTriggerEnter2DDel += (DelegateDefinitions.OnTriggerEnter2DType)System.Delegate.CreateDelegate(typeof(DelegateDefinitions.OnTriggerEnter2DType), targetObject, method);
                    dc.onTriggerEnter2DDel.Assign(method);
                    break;
                case DelegateDefinitions.events.OnTriggerEnter:
                    //dc.onTriggerEnterDel -= (DelegateDefinitions.OnTriggerEnterType)System.Delegate.CreateDelegate(typeof(DelegateDefinitions.OnTriggerEnterType), targetObject, method);
                    //dc.onTriggerEnterDel += (DelegateDefinitions.OnTriggerEnterType)System.Delegate.CreateDelegate(typeof(DelegateDefinitions.OnTriggerEnterType), targetObject, method);
                    dc.onTriggerEnterDel.Assign(method);
                    break;
                case DelegateDefinitions.events.OnServerInitialized:
                    //dc.onServerInitializedDel -= (DelegateDefinitions.OnServerInitializedType)System.Delegate.CreateDelegate(typeof(DelegateDefinitions.OnServerInitializedType), targetObject, method);
                    //dc.onServerInitializedDel += (DelegateDefinitions.OnServerInitializedType)System.Delegate.CreateDelegate(typeof(DelegateDefinitions.OnServerInitializedType), targetObject, method);
                    dc.onServerInitializedDel.Assign(method);
                    break;
                case DelegateDefinitions.events.OnSerializeNetworkView:
                    //dc.onSerializeNetworkViewDel -= (DelegateDefinitions.OnSerializeNetworkViewType)System.Delegate.CreateDelegate(typeof(DelegateDefinitions.OnSerializeNetworkViewType), targetObject, method);
                    //dc.onSerializeNetworkViewDel += (DelegateDefinitions.OnSerializeNetworkViewType)System.Delegate.CreateDelegate(typeof(DelegateDefinitions.OnSerializeNetworkViewType), targetObject, method);
                    dc.onSerializeNetworkView.Assign(method);
                    break;
                case DelegateDefinitions.events.OnPlayerDisconnected:
                    //dc.onPlayerDisconnectedDel -= (DelegateDefinitions.OnPlayerDisconnectedType)System.Delegate.CreateDelegate(typeof(DelegateDefinitions.OnPlayerDisconnectedType), targetObject, method);
                    //dc.onPlayerDisconnectedDel += (DelegateDefinitions.OnPlayerDisconnectedType)System.Delegate.CreateDelegate(typeof(DelegateDefinitions.OnPlayerDisconnectedType), targetObject, method);
                    dc.onPlayerDisconnectedDel.Assign(method);
                    break;
                case DelegateDefinitions.events.OnPlayerConnected:
                    //dc.onPlayerConnectedDel -= (DelegateDefinitions.OnPlayerConnectedType)System.Delegate.CreateDelegate(typeof(DelegateDefinitions.OnPlayerConnectedType), targetObject, method);
                    //dc.onPlayerConnectedDel += (DelegateDefinitions.OnPlayerConnectedType)System.Delegate.CreateDelegate(typeof(DelegateDefinitions.OnPlayerConnectedType), targetObject, method);
                    dc.onPlayerConnectedDel.Assign(method);
                    break;
                case DelegateDefinitions.events.OnParticleCollision:
                    //dc.onParticleCollisionDel -= (DelegateDefinitions.OnParticleCollisionType)System.Delegate.CreateDelegate(typeof(DelegateDefinitions.OnParticleCollisionType), targetObject, method);
                    //dc.onParticleCollisionDel += (DelegateDefinitions.OnParticleCollisionType)System.Delegate.CreateDelegate(typeof(DelegateDefinitions.OnParticleCollisionType), targetObject, method);
                    dc.onParticleCollisionDel.Assign(method);
                    break;
                case DelegateDefinitions.events.OnNetworkInstantiate:
                    //dc.onNetworkInstantiateDel -= (DelegateDefinitions.OnNetworkInstantiateType)System.Delegate.CreateDelegate(typeof(DelegateDefinitions.OnNetworkInstantiateType), targetObject, method);
                    //dc.onNetworkInstantiateDel += (DelegateDefinitions.OnNetworkInstantiateType)System.Delegate.CreateDelegate(typeof(DelegateDefinitions.OnNetworkInstantiateType), targetObject, method);
                    dc.onNetworkInstantiateDel.Assign(method);
                    break;
                case DelegateDefinitions.events.OnMouseUpAsButton:
                    //dc.onMouseUpAsButtonDel -= (DelegateDefinitions.OnMouseUpAsButtonType)System.Delegate.CreateDelegate(typeof(DelegateDefinitions.OnMouseUpAsButtonType), targetObject, method);
                    //dc.onMouseUpAsButtonDel += (DelegateDefinitions.OnMouseUpAsButtonType)System.Delegate.CreateDelegate(typeof(DelegateDefinitions.OnMouseUpAsButtonType), targetObject, method);
                    dc.onMouseUpAsButtonDel.Assign(method);
                    break;
                case DelegateDefinitions.events.OnMouseUp:
                    //dc.onMouseUpDel -= (DelegateDefinitions.OnMouseUpType)System.Delegate.CreateDelegate(typeof(DelegateDefinitions.OnMouseUpType), targetObject, method);
                    //dc.onMouseUpDel += (DelegateDefinitions.OnMouseUpType)System.Delegate.CreateDelegate(typeof(DelegateDefinitions.OnMouseUpType), targetObject, method);
                    dc.onMouseUpDel.Assign(method);
                    break;
                case DelegateDefinitions.events.OnMouseOver:
                    //.onMouseOverDel -= (DelegateDefinitions.OnMouseOverType)System.Delegate.CreateDelegate(typeof(DelegateDefinitions.OnMouseOverType), targetObject, method);
                    //dc.onMouseOverDel += (DelegateDefinitions.OnMouseOverType)System.Delegate.CreateDelegate(typeof(DelegateDefinitions.OnMouseOverType), targetObject, method);
                    dc.onMouseOverDel.Assign(method);
                    break;
                case DelegateDefinitions.events.OnMouseExit:
                    //dc.onMouseExitDel -= (DelegateDefinitions.OnMouseExitType)System.Delegate.CreateDelegate(typeof(DelegateDefinitions.OnMouseExitType), targetObject, method);
                    //dc.onMouseExitDel += (DelegateDefinitions.OnMouseExitType)System.Delegate.CreateDelegate(typeof(DelegateDefinitions.OnMouseExitType), targetObject, method);
                    dc.onMouseExitDel.Assign(method);
                    break;
                case DelegateDefinitions.events.OnMouseEnter:
                    //dc.onMouseEnterDel -= (DelegateDefinitions.OnMouseEnterType)System.Delegate.CreateDelegate(typeof(DelegateDefinitions.OnMouseEnterType), targetObject, method);
                    //dc.onMouseEnterDel += (DelegateDefinitions.OnMouseEnterType)System.Delegate.CreateDelegate(typeof(DelegateDefinitions.OnMouseEnterType), targetObject, method);
                    dc.onMouseEnterDel.Assign(method);
                    break;
                case DelegateDefinitions.events.OnMouseDrag:
                    //dc.onMouseDragDel -= (DelegateDefinitions.OnMouseDragType)System.Delegate.CreateDelegate(typeof(DelegateDefinitions.OnMouseDragType), targetObject, method);
                    //dc.onMouseDragDel += (DelegateDefinitions.OnMouseDragType)System.Delegate.CreateDelegate(typeof(DelegateDefinitions.OnMouseDragType), targetObject, method);
                    dc.onMouseDragDel.Assign(method);
                    break;
                case DelegateDefinitions.events.OnMouseDown:
                    //dc.onMouseDownDel -= (DelegateDefinitions.OnMouseDownType)System.Delegate.CreateDelegate(typeof(DelegateDefinitions.OnMouseDownType), targetObject, method);
                    //dc.onMouseDownDel += (DelegateDefinitions.OnMouseDownType)System.Delegate.CreateDelegate(typeof(DelegateDefinitions.OnMouseDownType), targetObject, method);
                    dc.onMouseDownDel.Assign(method);
                    break;
                case DelegateDefinitions.events.OnMasterServerEvent:
                    //dc.onMasterServerEventDel -= (DelegateDefinitions.OnMasterServerEventType)System.Delegate.CreateDelegate(typeof(DelegateDefinitions.OnMasterServerEventType), targetObject, method);
                    //dc.onMasterServerEventDel += (DelegateDefinitions.OnMasterServerEventType)System.Delegate.CreateDelegate(typeof(DelegateDefinitions.OnMasterServerEventType), targetObject, method);
                    dc.onMasterServerEvent.Assign(method);
                    break;
                case DelegateDefinitions.events.OnLevelWasLoaded:
                    //dc.onLevelWasLoadedDel -= (DelegateDefinitions.OnLevelWasLoadedType)System.Delegate.CreateDelegate(typeof(DelegateDefinitions.OnLevelWasLoadedType), targetObject, method);
                    //dc.onLevelWasLoadedDel += (DelegateDefinitions.OnLevelWasLoadedType)System.Delegate.CreateDelegate(typeof(DelegateDefinitions.OnLevelWasLoadedType), targetObject, method);
                    dc.onLevelWasLoadedDel.Assign(method);
                    break;
                case DelegateDefinitions.events.OnJointBreak:
                    //dc.onJointBreakDel -= (DelegateDefinitions.OnJointBreakType)System.Delegate.CreateDelegate(typeof(DelegateDefinitions.OnJointBreakType), targetObject, method);
                    //dc.onJointBreakDel += (DelegateDefinitions.OnJointBreakType)System.Delegate.CreateDelegate(typeof(DelegateDefinitions.OnJointBreakType), targetObject, method);
                    dc.onJointBreakDel.Assign(method);
                    break;
                case DelegateDefinitions.events.OnFailedToConnectToMasterServer:
                    //dc.onFailedToConnectToMasterServerDel -= (DelegateDefinitions.OnFailedToConnectToMasterServerType)System.Delegate.CreateDelegate(typeof(DelegateDefinitions.OnFailedToConnectToMasterServerType), targetObject, method);
                    //dc.onFailedToConnectToMasterServerDel += (DelegateDefinitions.OnFailedToConnectToMasterServerType)System.Delegate.CreateDelegate(typeof(DelegateDefinitions.OnFailedToConnectToMasterServerType), targetObject, method);
                    dc.onFailedToConnectToMasterServerDel.Assign(method);
                    break;
                case DelegateDefinitions.events.OnFailedToConnect:
                    //dc.onFailedToConnectDel -= (DelegateDefinitions.OnFailedToConnectType)System.Delegate.CreateDelegate(typeof(DelegateDefinitions.OnFailedToConnectType), targetObject, method);
                    //dc.onFailedToConnectDel += (DelegateDefinitions.OnFailedToConnectType)System.Delegate.CreateDelegate(typeof(DelegateDefinitions.OnFailedToConnectType), targetObject, method);
                    dc.onFailedToConnectType.Assign(method);
                    break;
                case DelegateDefinitions.events.OnEnable:
                    //dc.onEnableDel -= (DelegateDefinitions.OnEnableType)System.Delegate.CreateDelegate(typeof(DelegateDefinitions.OnEnableType), targetObject, method);
                    //dc.onEnableDel += (DelegateDefinitions.OnEnableType)System.Delegate.CreateDelegate(typeof(DelegateDefinitions.OnEnableType), targetObject, method);
                    dc.onEnableDel.Assign(method);
                    break;
                case DelegateDefinitions.events.OnDisconnectedFromServer:
                    //dc.onDisconnectedFromServerDel -= (DelegateDefinitions.OnDisconnectedFromServerType)System.Delegate.CreateDelegate(typeof(DelegateDefinitions.OnDisconnectedFromServerType), targetObject, method);
                    //dc.onDisconnectedFromServerDel += (DelegateDefinitions.OnDisconnectedFromServerType)System.Delegate.CreateDelegate(typeof(DelegateDefinitions.OnDisconnectedFromServerType), targetObject, method);
                    dc.onDisconnectedFromServerDel.Assign(method);
                    break;
                case DelegateDefinitions.events.OnDisable:
                    //dc.onDisableDel -= (DelegateDefinitions.OnDisableType)System.Delegate.CreateDelegate(typeof(DelegateDefinitions.OnDisableType), targetObject, method);
                    //dc.onDisableDel += (DelegateDefinitions.OnDisableType)System.Delegate.CreateDelegate(typeof(DelegateDefinitions.OnDisableType), targetObject, method);
                    dc.onDisableDel.Assign(method);
                    break;
                case DelegateDefinitions.events.OnDestroy:
                    //dc.onDestroyDel -= (DelegateDefinitions.OnDestroyType)System.Delegate.CreateDelegate(typeof(DelegateDefinitions.OnDestroyType), targetObject, method);
                    //dc.onDestroyDel += (DelegateDefinitions.OnDestroyType)System.Delegate.CreateDelegate(typeof(DelegateDefinitions.OnDestroyType), targetObject, method);
                    dc.onDestroyDel.Assign(method);
                    break;
                case DelegateDefinitions.events.OnControllerColliderHit:
                    //dc.onControllerColliderHitDel -= (DelegateDefinitions.OnControllerColliderHitType)System.Delegate.CreateDelegate(typeof(DelegateDefinitions.OnControllerColliderHitType), targetObject, method);
                    //dc.onControllerColliderHitDel += (DelegateDefinitions.OnControllerColliderHitType)System.Delegate.CreateDelegate(typeof(DelegateDefinitions.OnControllerColliderHitType), targetObject, method);
                    dc.onControllerColliderHitType.Assign(method);
                    break;
                case DelegateDefinitions.events.OnConnectedToServer:
                    //dc.onConnectedToServerDel -= (DelegateDefinitions.OnConnectedToServerType)System.Delegate.CreateDelegate(typeof(DelegateDefinitions.OnConnectedToServerType), targetObject, method);
                    //dc.onConnectedToServerDel += (DelegateDefinitions.OnConnectedToServerType)System.Delegate.CreateDelegate(typeof(DelegateDefinitions.OnConnectedToServerType), targetObject, method);
                    dc.onConnectedToServerDel.Assign(method);
                    break;
                case DelegateDefinitions.events.OnCollisionStay2D:
                    //dc.onCollisionStay2DDel -= (DelegateDefinitions.OnCollisionStay2DType)System.Delegate.CreateDelegate(typeof(DelegateDefinitions.OnCollisionStay2DType), targetObject, method);
                    //dc.onCollisionStay2DDel += (DelegateDefinitions.OnCollisionStay2DType)System.Delegate.CreateDelegate(typeof(DelegateDefinitions.OnCollisionStay2DType), targetObject, method);
                    dc.onCollisionStay2DDel.Assign(method);
                    break;
                case DelegateDefinitions.events.OnCollisionStay:
                    //dc.onCollisionStayDel -= (DelegateDefinitions.OnCollisionStayType)System.Delegate.CreateDelegate(typeof(DelegateDefinitions.OnCollisionStayType), targetObject, method);
                    //dc.onCollisionStayDel += (DelegateDefinitions.OnCollisionStayType)System.Delegate.CreateDelegate(typeof(DelegateDefinitions.OnCollisionStayType), targetObject, method);
                    dc.onCollisionStayDel.Assign(method);
                    break;
                case DelegateDefinitions.events.OnCollisionExit2D:
                    //dc.onCollisionExit2DDel -= (DelegateDefinitions.OnCollisionExit2DType)System.Delegate.CreateDelegate(typeof(DelegateDefinitions.OnCollisionExit2DType), targetObject, method);
                    //dc.onCollisionExit2DDel += (DelegateDefinitions.OnCollisionExit2DType)System.Delegate.CreateDelegate(typeof(DelegateDefinitions.OnCollisionExit2DType), targetObject, method);
                    dc.onCollisionExit2DDel.Assign(method);
                    break;
                case DelegateDefinitions.events.OnCollisionExit:
                    //dc.onCollisionExitDel -= (DelegateDefinitions.OnCollisionExitType)System.Delegate.CreateDelegate(typeof(DelegateDefinitions.OnCollisionExitType), targetObject, method);
                    //dc.onCollisionExitDel += (DelegateDefinitions.OnCollisionExitType)System.Delegate.CreateDelegate(typeof(DelegateDefinitions.OnCollisionExitType), targetObject, method);
                    dc.onCollisionExitDel.Assign(method);
                    break;
                case DelegateDefinitions.events.OnCollisionEnter2D:
                    //dc.onCollisionEnter2DDel -= (DelegateDefinitions.OnCollisionEnter2DType)System.Delegate.CreateDelegate(typeof(DelegateDefinitions.OnCollisionEnter2DType), targetObject, method);
                    //dc.onCollisionEnter2DDel += (DelegateDefinitions.OnCollisionEnter2DType)System.Delegate.CreateDelegate(typeof(DelegateDefinitions.OnCollisionEnter2DType), targetObject, method);
                    dc.onCollisionEnter2DDel.Assign(method);
                    break;
                case DelegateDefinitions.events.OnCollisionEnter:
                    //dc.onCollisionEnterDel -= (DelegateDefinitions.OnCollisionEnterType)System.Delegate.CreateDelegate(typeof(DelegateDefinitions.OnCollisionEnterType), targetObject, method);
                    //dc.onCollisionEnterDel += (DelegateDefinitions.OnCollisionEnterType)System.Delegate.CreateDelegate(typeof(DelegateDefinitions.OnCollisionEnterType), targetObject, method);
                    dc.onCollisionEnterDel.Assign(method);
                    break;
                case DelegateDefinitions.events.OnBecameVisible:
                    //dc.onBecameVisibleDel -= (DelegateDefinitions.OnBecameVisibleType)System.Delegate.CreateDelegate(typeof(DelegateDefinitions.OnBecameVisibleType), targetObject, method);
                    //dc.onBecameVisibleDel += (DelegateDefinitions.OnBecameVisibleType)System.Delegate.CreateDelegate(typeof(DelegateDefinitions.OnBecameVisibleType), targetObject, method);
                    dc.onBecameVisibleDel.Assign(method);
                    break;
                case DelegateDefinitions.events.OnBecameInvisible:
                    //dc.onBecameInvisibleDel -= (DelegateDefinitions.OnBecameInvisibleType)System.Delegate.CreateDelegate(typeof(DelegateDefinitions.OnBecameInvisibleType), targetObject, method);
                    //dc.onBecameInvisibleDel += (DelegateDefinitions.OnBecameInvisibleType)System.Delegate.CreateDelegate(typeof(DelegateDefinitions.OnBecameInvisibleType), targetObject, method);
                    dc.onBecameInvisibleDel.Assign(method);
                    break;
                case DelegateDefinitions.events.OnAudioFilterRead:
                    //dc.onAudioFilterReadDel -= (DelegateDefinitions.OnAudioFilterReadType)System.Delegate.CreateDelegate(typeof(DelegateDefinitions.OnAudioFilterReadType), targetObject, method);
                    //dc.onAudioFilterReadDel += (DelegateDefinitions.OnAudioFilterReadType)System.Delegate.CreateDelegate(typeof(DelegateDefinitions.OnAudioFilterReadType), targetObject, method);
                    dc.onAudioFilterReadDel.Assign(method);
                    break;
                case DelegateDefinitions.events.OnApplicationQuit:
                    //dc.onApplicationQuitDel -= (DelegateDefinitions.OnApplicationQuitType)System.Delegate.CreateDelegate(typeof(DelegateDefinitions.OnApplicationQuitType), targetObject, method);
                    //dc.onApplicationQuitDel += (DelegateDefinitions.OnApplicationQuitType)System.Delegate.CreateDelegate(typeof(DelegateDefinitions.OnApplicationQuitType), targetObject, method);
                    dc.onApplicationQuitDel.Assign(method);
                    break;
                case DelegateDefinitions.events.OnApplicationPause:
                    //dc.onApplicationPauseDel -= (DelegateDefinitions.OnApplicationPauseType)System.Delegate.CreateDelegate(typeof(DelegateDefinitions.OnApplicationPauseType), targetObject, method);
                    //dc.onApplicationPauseDel += (DelegateDefinitions.OnApplicationPauseType)System.Delegate.CreateDelegate(typeof(DelegateDefinitions.OnApplicationPauseType), targetObject, method);
                    dc.onApplicationPauseDel.Assign(method);
                    break;
                case DelegateDefinitions.events.OnapplicationFocus:
                    //dc.onapplicationFocusDel -= (DelegateDefinitions.OnapplicationFocusType)System.Delegate.CreateDelegate(typeof(DelegateDefinitions.OnapplicationFocusType), targetObject, method);
                    //dc.onapplicationFocusDel += (DelegateDefinitions.OnapplicationFocusType)System.Delegate.CreateDelegate(typeof(DelegateDefinitions.OnapplicationFocusType), targetObject, method);
                    dc.onApplicationFocusDel.Assign(method);
                    break;
                case DelegateDefinitions.events.OnAnimatorMove:
                    //dc.onAnimatorMoveDel -= (DelegateDefinitions.OnAnimatorMoveType)System.Delegate.CreateDelegate(typeof(DelegateDefinitions.OnAnimatorMoveType), targetObject, method);
                    //dc.onAnimatorMoveDel += (DelegateDefinitions.OnAnimatorMoveType)System.Delegate.CreateDelegate(typeof(DelegateDefinitions.OnAnimatorMoveType), targetObject, method);
                    dc.onAnimatorMoveDel.Assign(method);
                    break;
                case DelegateDefinitions.events.OnAnimatorIK:
                    //dc.onAnimatorIKDel -= (DelegateDefinitions.OnAnimatorIKType)System.Delegate.CreateDelegate(typeof(DelegateDefinitions.OnAnimatorIKType), targetObject, method);
                    //dc.onAnimatorIKDel += (DelegateDefinitions.OnAnimatorIKType)System.Delegate.CreateDelegate(typeof(DelegateDefinitions.OnAnimatorIKType), targetObject, method);
                    dc.onAnimatorIKDel.Assign(method);
                    break;
                case DelegateDefinitions.events.LateUpdate:
                    //dc.lateUpdateDel -= (DelegateDefinitions.LateUpdateType)System.Delegate.CreateDelegate(typeof(DelegateDefinitions.LateUpdateType), targetObject, method);
                    //dc.lateUpdateDel += (DelegateDefinitions.LateUpdateType)System.Delegate.CreateDelegate(typeof(DelegateDefinitions.LateUpdateType), targetObject, method);
                    dc.lateUpdateDel.Assign(method);
                    break;
                case DelegateDefinitions.events.FixedUpdate:
                    //dc.fixedUpdateDel -= (DelegateDefinitions.FixedUpdateType)System.Delegate.CreateDelegate(typeof(DelegateDefinitions.FixedUpdateType), targetObject, method);
                    //dc.fixedUpdateDel += (DelegateDefinitions.FixedUpdateType)System.Delegate.CreateDelegate(typeof(DelegateDefinitions.FixedUpdateType), targetObject, method);
                    dc.fixedUpdateDel.Assign(method);
                    break;
                case DelegateDefinitions.events.Awake:
                    //dc.awakeDel -= (DelegateDefinitions.AwakeType)System.Delegate.CreateDelegate(typeof(DelegateDefinitions.AwakeType), targetObject, method);
                    //dc.awakeDel += (DelegateDefinitions.AwakeType)System.Delegate.CreateDelegate(typeof(DelegateDefinitions.AwakeType), targetObject, method);
                    dc.awakeDel.Assign(method);
                    break;
                case DelegateDefinitions.events.OnRenderObject:
                    //dc.onRenderObjectDel -= (DelegateDefinitions.OnRenderObjectType)System.Delegate.CreateDelegate(typeof(DelegateDefinitions.OnRenderObjectType), targetObject, method);
                    //dc.onRenderObjectDel += (DelegateDefinitions.OnRenderObjectType)System.Delegate.CreateDelegate(typeof(DelegateDefinitions.OnRenderObjectType), targetObject, method);
                    dc.OnRenderObjectDel.Assign(method);
                    break;
                case DelegateDefinitions.events.OnRenderImage:
                    //dc.onRenderImageDel -= (DelegateDefinitions.OnRenderImageType)System.Delegate.CreateDelegate(typeof(DelegateDefinitions.OnRenderImageType), targetObject, method);
                    //dc.onRenderImageDel += (DelegateDefinitions.OnRenderImageType)System.Delegate.CreateDelegate(typeof(DelegateDefinitions.OnRenderImageType), targetObject, method);
                    dc.onRenderImageDel.Assign(method);
                    break;
                case DelegateDefinitions.events.OnPreRender:
                    //dc.onPreRenderDel -= (DelegateDefinitions.OnPreRenderType)System.Delegate.CreateDelegate(typeof(DelegateDefinitions.OnPreRenderType), targetObject, method);
                    //dc.onPreRenderDel += (DelegateDefinitions.OnPreRenderType)System.Delegate.CreateDelegate(typeof(DelegateDefinitions.OnPreRenderType), targetObject, method);
                    dc.onPreRenderTypeDel.Assign(method);
                    break;
                case DelegateDefinitions.events.OnPreCull:
                    //dc.onPreCullDel -= (DelegateDefinitions.OnPreCullType)System.Delegate.CreateDelegate(typeof(DelegateDefinitions.OnPreCullType), targetObject, method);
                    //dc.onPreCullDel += (DelegateDefinitions.OnPreCullType)System.Delegate.CreateDelegate(typeof(DelegateDefinitions.OnPreCullType), targetObject, method);
                    dc.onPreCullDel.Assign(method);
                    break;
                case DelegateDefinitions.events.OnPostRender:
                    //dc.onPostRenderDel -= (DelegateDefinitions.OnPostRenderType)System.Delegate.CreateDelegate(typeof(DelegateDefinitions.OnPostRenderType), targetObject, method);
                    //dc.onPostRenderDel += (DelegateDefinitions.OnPostRenderType)System.Delegate.CreateDelegate(typeof(DelegateDefinitions.OnPostRenderType), targetObject, method);
                    dc.onPostRenderDel.Assign(method);
                    break;
                case DelegateDefinitions.events.OnWillRenderObject:
                    //dc.onWillRenderObjectDel -= (DelegateDefinitions.OnWillRenderObjectType)System.Delegate.CreateDelegate(typeof(DelegateDefinitions.OnWillRenderObjectType), targetObject, method);
                    //dc.onWillRenderObjectDel += (DelegateDefinitions.OnWillRenderObjectType)System.Delegate.CreateDelegate(typeof(DelegateDefinitions.OnWillRenderObjectType), targetObject, method);
                    dc.onWillRenderObjectDel.Assign(method);
                    break;
                case DelegateDefinitions.events.OnGUI:
                    //dc.onGUIDel -= (DelegateDefinitions.OnGUIType)System.Delegate.CreateDelegate(typeof(DelegateDefinitions.OnGUIType), targetObject, method);
                    //dc.onGUIDel += (DelegateDefinitions.OnGUIType)System.Delegate.CreateDelegate(typeof(DelegateDefinitions.OnGUIType), targetObject, method);
                    dc.onGUIDel.Assign(method);
                    break;
                case DelegateDefinitions.events.OnDrawGizmosSelected:
                    //dc.onDrawGizmosSelectedDel -= (DelegateDefinitions.OnDrawGizmosSelectedType)System.Delegate.CreateDelegate(typeof(DelegateDefinitions.OnDrawGizmosSelectedType), targetObject, method);
                    //dc.onDrawGizmosSelectedDel += (DelegateDefinitions.OnDrawGizmosSelectedType)System.Delegate.CreateDelegate(typeof(DelegateDefinitions.OnDrawGizmosSelectedType), targetObject, method);
                    dc.onDrawGizmosSelectedDel.Assign(method);
                    break;
                case DelegateDefinitions.events.OnDrawGizmos:
                    //dc.onDrawGizmosDel -= (DelegateDefinitions.OnDrawGizmosType)System.Delegate.CreateDelegate(typeof(DelegateDefinitions.OnDrawGizmosType), targetObject, method);
                    //dc.onDrawGizmosDel += (DelegateDefinitions.OnDrawGizmosType)System.Delegate.CreateDelegate(typeof(DelegateDefinitions.OnDrawGizmosType), targetObject, method);
                    dc.onDrawGizmosDel.Assign(targetObject, method);
                    break;
               
            }
        }*/

        #endregion

        /// <summary>
        /// Override this method to return the variables you wish to make subscribeable. Other classes
        /// can then subscribe to be notified when those variables change. Caching the dictionary you
        /// are returning is recommended, this method will be called often
        /// </summary>
        /// <returns></returns>
        protected virtual Dictionary<string, object> IdentifySubscribeableVariables() {
            return new Dictionary<string, object>();
        }

        /// <summary>
        /// Updates other classes that have registered for variable changes. In practice, you will never
        /// need to call this, it is called autmatically every frame.
        /// </summary>
        public void RefreshSubscribers() {
            changeSubscription.RefreshSubscribers(IdentifySubscribeableVariables());
        }

        #region Message Functions

        private void Update() {
            CheckSetup();
            if (methodList != null)
            {
                // getting the list of Update methods
                Method[] methods = methodList.GetMethod(DelegateDefinitions.events.Update);

                // Setting up if necessary
                if (!dc.updateDel.Assigned && methods.Length > 0)
                {
                    dc.updateDel.Assign(methods);
                }

                // Calling if delegate exists
                if (dc.updateDel.Assigned)
                    dc.updateDel.Execute();
            }
            if (model != null){
               model.RefreshSubscribers();
            }
        }

        private void Start() {
            CheckSetup();//TODO Check if this still works

            if (methodList != null)
            {
                // getting the list of Update methods
                Method[] methods = methodList.GetMethod(DelegateDefinitions.events.Start);

                // Setting up if necessary
                if (!dc.startDel.Assigned && methods.Length > 0)
                {
                    dc.startDel.Assign(methods);
                }

                // Calling if delegate exists
                if (dc.startDel.Assigned)
                    dc.startDel.Execute();
            }
            //TODO Implement
        }

        public override void Reset() {
            MakeReady();

            if (methodList != null)
            {
                // getting the list of Update methods
                Method[] methods = methodList.GetMethod(DelegateDefinitions.events.Reset);

                // Setting up if necessary
                if (!dc.resetDel.Assigned && methods.Length > 0)
                {
                    dc.resetDel.Assign(methods);
                }

                // Calling if delegate exists
                if (dc.resetDel.Assigned)
                    dc.resetDel.Execute();
            }
        }

        private void OnWillRenderObject() {
            CheckSetup();

            if (methodList != null)
            {
                // getting the list of Update methods
                Method[] methods = methodList.GetMethod(DelegateDefinitions.events.OnWillRenderObject);

                // Setting up if necessary
                if (!dc.onWillRenderObjectDel.Assigned && methods.Length > 0)
                {
                    dc.onWillRenderObjectDel.Assign(methods);
                }

                // Calling if delegate exists
                if (dc.onWillRenderObjectDel.Assigned)
                    dc.onWillRenderObjectDel.Execute();
            }
        }

        private void OnValidate() {
            CheckSetup();
            MakeReady();

            if (methodList != null)
            {
                // getting the list of Update methods
                Method[] methods = methodList.GetMethod(DelegateDefinitions.events.OnValidate);

                // Setting up if necessary
                if (!dc.onValidateDel.Assigned && methods.Length > 0)
                {
                    dc.onValidateDel.Assign(methods);
                }

                // Calling if delegate exists
                if (dc.onValidateDel.Assigned)
                    dc.onValidateDel.Execute();
            }

        }

        private void OnTriggerStay2D(Collider2D other) {
            CheckSetup();

            if (methodList != null)
            {
                // getting the list of Update methods
                Method[] methods = methodList.GetMethod(DelegateDefinitions.events.OnTriggerStay2D);

                // Setting up if necessary
                if (!dc.onTriggerStay2DDel.Assigned && methods.Length > 0)
                {
                    dc.updateDel.Assign(methods);
                }

                // Calling if delegate exists
                if (dc.onTriggerStay2DDel.Assigned)
                    dc.onTriggerStay2DDel.Execute(other);
            }
        }

        private void OnTriggerStay(Collider other) {
            CheckSetup();

            if (methodList != null)
            {
                // getting the list of Update methods
                Method[] methods = methodList.GetMethod(DelegateDefinitions.events.OnTriggerStay);

                // Setting up if necessary
                if (!dc.onTriggerStayDel.Assigned && methods.Length > 0)
                {
                    dc.onTriggerStayDel.Assign(methods);
                }

                // Calling if delegate exists
                if (dc.onTriggerStayDel.Assigned)
                    dc.onTriggerStayDel.Execute(other);
            }

        }

        private void OnTriggerExit2D(Collider2D other) {
            CheckSetup();

            if (methodList != null)
            {
                // getting the list of Update methods
                Method[] methods = methodList.GetMethod(DelegateDefinitions.events.Update);

                // Setting up if necessary
                if (!dc.updateDel.Assigned && methods.Length > 0)
                {
                    dc.updateDel.Assign(methods);
                }

                // Calling if delegate exists
                if (dc.updateDel.Assigned)
                    dc.updateDel.Execute();
            }

        }

        private void OnTriggerExit(Collider other) {
            CheckSetup();

            if (methodList != null)
            {
                // getting the list of Update methods
                Method[] methods = methodList.GetMethod(DelegateDefinitions.events.OnTriggerExit);

                // Setting up if necessary
                if (!dc.onTriggerExitDel.Assigned && methods.Length > 0)
                {
                    dc.onTriggerExitDel.Assign(methods);
                }

                // Calling if delegate exists
                if (dc.onTriggerExitDel.Assigned)
                    dc.onTriggerExitDel.Execute(other);
            }

        }

        private void OnTriggerEnter2D(Collider2D other) {
            CheckSetup();

            if (methodList != null)
            {
                // getting the list of Update methods
                Method[] methods = methodList.GetMethod(DelegateDefinitions.events.OnTriggerEnter2D);

                // Setting up if necessary
                if (!dc.onTriggerEnter2DDel.Assigned && methods.Length > 0)
                {
                    dc.onTriggerEnter2DDel.Assign(methods);
                }

                // Calling if delegate exists
                if (dc.onTriggerEnter2DDel.Assigned)
                    dc.onTriggerEnter2DDel.Execute(other);
            }

        }

        private void OnTriggerEnter(Collider other) {
            CheckSetup();

            if (methodList != null)
            {
                // getting the list of Update methods
                Method[] methods = methodList.GetMethod(DelegateDefinitions.events.OnTriggerEnter);

                // Setting up if necessary
                if (!dc.onTriggerEnterDel.Assigned && methods.Length > 0)
                {
                    dc.onTriggerEnterDel.Assign(methods);
                }

                // Calling if delegate exists
                if (dc.onTriggerEnterDel.Assigned)
                    dc.onTriggerEnterDel.Execute(other);
            }

        }

        private void OnServerInitialized() {
            CheckSetup();

            if (methodList != null)
            {
                // getting the list of Update methods
                Method[] methods = methodList.GetMethod(DelegateDefinitions.events.OnServerInitialized);

                // Setting up if necessary
                if (!dc.onServerInitializedDel.Assigned && methods.Length > 0)
                {
                    dc.onServerInitializedDel.Assign(methods);
                }

                // Calling if delegate exists
                if (dc.onServerInitializedDel.Assigned)
                    dc.onServerInitializedDel.Execute();
            }
        }

        /*
        private void OnSerializeNetworkView(NetworkMessageInfo info, BitStream stream) {
            //TODO Implement
        }*/
        
        private void OnRenderObject() {
            CheckSetup();

            if (methodList != null)
            {
                // getting the list of Update methods
                Method[] methods = methodList.GetMethod(DelegateDefinitions.events.OnRenderObject);

                // Setting up if necessary
                if (!dc.OnRenderObjectDel.Assigned && methods.Length > 0)
                {
                    dc.OnRenderObjectDel.Assign(methods);
                }

                // Calling if delegate exists
                if (dc.OnRenderObjectDel.Assigned)
                    dc.OnRenderObjectDel.Execute();
            }
        }

        private void OnRenderImage(RenderTexture destination, RenderTexture source) {
            CheckSetup();

            if (methodList != null)
            {
                // getting the list of Update methods
                Method[] methods = methodList.GetMethod(DelegateDefinitions.events.OnRenderImage);

                // Setting up if necessary
                if (!dc.onRenderImageDel.Assigned && methods.Length > 0)
                {
                    dc.onRenderImageDel.Assign(methods);
                }

                // Calling if delegate exists
                if (dc.onRenderImageDel.Assigned)
                    dc.onRenderImageDel.Execute(destination, source);
            }
        }

        private void OnPreRender() {
            CheckSetup();

            if (methodList != null)
            {
                // getting the list of Update methods
                Method[] methods = methodList.GetMethod(DelegateDefinitions.events.OnPreRender);

                // Setting up if necessary
                if (!dc.onPreRenderTypeDel.Assigned && methods.Length > 0)
                {
                    dc.onPreRenderTypeDel.Assign(methods);
                }

                // Calling if delegate exists
                if (dc.onPreRenderTypeDel.Assigned)
                    dc.onPreRenderTypeDel.Execute();
            }
        }

        private void OnPreCull() {
            CheckSetup();

            if (methodList != null)
            {
                // getting the list of Update methods
                Method[] methods = methodList.GetMethod(DelegateDefinitions.events.OnPreCull);

                // Setting up if necessary
                if (!dc.onPreCullDel.Assigned && methods.Length > 0)
                {
                    dc.onPreCullDel.Assign(methods);
                }

                // Calling if delegate exists
                if (dc.onPreCullDel.Assigned)
                    dc.onPreCullDel.Execute();
            }
        }

        private void OnPostRender() {
            CheckSetup();

            if (methodList != null)
            {
                // getting the list of Update methods
                Method[] methods = methodList.GetMethod(DelegateDefinitions.events.OnPostRender);

                // Setting up if necessary
                if (!dc.onPostRenderDel.Assigned && methods.Length > 0)
                {
                    dc.onPostRenderDel.Assign(methods);
                }

                // Calling if delegate exists
                if (dc.onPostRenderDel.Assigned)
                    dc.onPostRenderDel.Execute();
            }
        }

        private void OnPlayerDisconnected(NetworkPlayer player) {
            CheckSetup();

            if (methodList != null)
            {
                // getting the list of Update methods
                Method[] methods = methodList.GetMethod(DelegateDefinitions.events.OnPlayerDisconnected);

                // Setting up if necessary
                if (!dc.onPlayerDisconnectedDel.Assigned && methods.Length > 0)
                {
                    dc.onPlayerDisconnectedDel.Assign(methods);
                }

                // Calling if delegate exists
                if (dc.onPlayerDisconnectedDel.Assigned)
                    dc.onPlayerDisconnectedDel.Execute(player);
            }
        }

        private void OnPlayerConnected(NetworkPlayer player) {
            CheckSetup();

            if (methodList != null)
            {
                // getting the list of Update methods
                Method[] methods = methodList.GetMethod(DelegateDefinitions.events.OnPlayerConnected);

                // Setting up if necessary
                if (!dc.onPlayerConnectedDel.Assigned && methods.Length > 0)
                {
                    dc.onPlayerConnectedDel.Assign(methods);
                }

                // Calling if delegate exists
                if (dc.onPlayerConnectedDel.Assigned)
                    dc.onPlayerConnectedDel.Execute(player);
            }
        }

        private void OnParticleCollision(GameObject other) {
            CheckSetup();

            if (methodList != null)
            {
                // getting the list of Update methods
                Method[] methods = methodList.GetMethod(DelegateDefinitions.events.OnParticleCollision);

                // Setting up if necessary
                if (!dc.onParticleCollisionDel.Assigned && methods.Length > 0)
                {
                    dc.onParticleCollisionDel.Assign(methods);
                }

                // Calling if delegate exists
                if (dc.onParticleCollisionDel.Assigned)
                    dc.onParticleCollisionDel.Execute(other);
            }
        }

        private void OnNetworkInstantiate(NetworkMessageInfo info) {
            CheckSetup();

            if (methodList != null)
            {
                // getting the list of Update methods
                Method[] methods = methodList.GetMethod(DelegateDefinitions.events.OnNetworkInstantiate);

                // Setting up if necessary
                if (!dc.onNetworkInstantiateDel.Assigned && methods.Length > 0)
                {
                    dc.onNetworkInstantiateDel.Assign(methods);
                }

                // Calling if delegate exists
                if (dc.onNetworkInstantiateDel.Assigned)
                    dc.onNetworkInstantiateDel.Execute(info);
            }
        }

        private void OnMouseUpAsButton() {
            CheckSetup();

            if (methodList != null)
            {
                // getting the list of Update methods
                Method[] methods = methodList.GetMethod(DelegateDefinitions.events.OnMouseUpAsButton);

                // Setting up if necessary
                if (!dc.onMouseUpAsButtonDel.Assigned && methods.Length > 0)
                {
                    dc.onMouseUpAsButtonDel.Assign(methods);
                }

                // Calling if delegate exists
                if (dc.onMouseUpAsButtonDel.Assigned)
                    dc.onMouseUpAsButtonDel.Execute();
            }
        }

        private void OnMouseUp() {
            CheckSetup();

            if (methodList != null)
            {
                // getting the list of Update methods
                Method[] methods = methodList.GetMethod(DelegateDefinitions.events.OnMouseUp);

                // Setting up if necessary
                if (!dc.onMouseUpDel.Assigned && methods.Length > 0)
                {
                    dc.onMouseUpDel.Assign(methods);
                }

                // Calling if delegate exists
                if (dc.onMouseUpDel.Assigned)
                    dc.onMouseUpDel.Execute();
            }
        }

        private void OnMouseOver() {
            CheckSetup();

            if (methodList != null)
            {
                // getting the list of Update methods
                Method[] methods = methodList.GetMethod(DelegateDefinitions.events.OnMouseOver);

                // Setting up if necessary
                if (!dc.onMouseOverDel.Assigned && methods.Length > 0)
                {
                    dc.onMouseOverDel.Assign(methods);
                }

                // Calling if delegate exists
                if (dc.onMouseOverDel.Assigned)
                    dc.onMouseOverDel.Execute();
            }
        }

        private void OnMouseExit() {
            CheckSetup();

            if (methodList != null)
            {
                // getting the list of Update methods
                Method[] methods = methodList.GetMethod(DelegateDefinitions.events.OnMouseExit);

                // Setting up if necessary
                if (!dc.onMouseExitDel.Assigned && methods.Length > 0)
                {
                    dc.onMouseExitDel.Assign(methods);
                }

                // Calling if delegate exists
                if (dc.onMouseExitDel.Assigned)
                    dc.onMouseExitDel.Execute();
            }
        }

        private void OnMouseEnter() {
            CheckSetup();

            if (methodList != null)
            {
                // getting the list of Update methods
                Method[] methods = methodList.GetMethod(DelegateDefinitions.events.OnMouseEnter);

                // Setting up if necessary
                if (!dc.onMouseEnterDel.Assigned && methods.Length > 0)
                {
                    dc.onMouseEnterDel.Assign(methods);
                }

                // Calling if delegate exists
                if (dc.onMouseEnterDel.Assigned)
                    dc.onMouseEnterDel.Execute();
            }
        }

        private void OnMouseDrag() {
            CheckSetup();

            if (methodList != null)
            {
                // getting the list of Update methods
                Method[] methods = methodList.GetMethod(DelegateDefinitions.events.OnMouseDrag);

                // Setting up if necessary
                if (!dc.onMouseDragDel.Assigned && methods.Length > 0)
                {
                    dc.onMouseDragDel.Assign(methods);
                }

                // Calling if delegate exists
                if (dc.onMouseDragDel.Assigned)
                    dc.onMouseDragDel.Execute();
            }
        }

        private void OnMouseDown() {
            CheckSetup();

            if (methodList != null)
            {
                // getting the list of Update methods
                Method[] methods = methodList.GetMethod(DelegateDefinitions.events.OnMouseDown);

                // Setting up if necessary
                if (!dc.onMouseDownDel.Assigned && methods.Length > 0)
                {
                    dc.onMouseDownDel.Assign(methods);
                }

                // Calling if delegate exists
                if (dc.onMouseDownDel.Assigned)
                    dc.onMouseDownDel.Execute();
            }
        }

        private void OnMasterServerEvent(MasterServerEvent msEvent) {
            CheckSetup();

            if (methodList != null)
            {
                // getting the list of Update methods
                Method[] methods = methodList.GetMethod(DelegateDefinitions.events.OnMasterServerEvent);

                // Setting up if necessary
                if (!dc.onMasterServerEvent.Assigned && methods.Length > 0)
                {
                    dc.onMasterServerEvent.Assign(methods);
                }

                // Calling if delegate exists
                if (dc.onMasterServerEvent.Assigned)
                    dc.onMasterServerEvent.Execute(msEvent);
            }
        }

        private void OnLevelWasLoaded(int level) {
            CheckSetup();

            if (methodList != null)
            {
                // getting the list of Update methods
                Method[] methods = methodList.GetMethod(DelegateDefinitions.events.OnLevelWasLoaded);

                // Setting up if necessary
                if (!dc.onLevelWasLoadedDel.Assigned && methods.Length > 0)
                {
                    dc.onLevelWasLoadedDel.Assign(methods);
                }

                // Calling if delegate exists
                if (dc.onLevelWasLoadedDel.Assigned)
                    dc.onLevelWasLoadedDel.Execute(level);
            }
        }

        private void OnJointBreak(float breakForce) {
            CheckSetup();

            if (methodList != null)
            {
                // getting the list of Update methods
                Method[] methods = methodList.GetMethod(DelegateDefinitions.events.OnJointBreak);

                // Setting up if necessary
                if (!dc.onJointBreakDel.Assigned && methods.Length > 0)
                {
                    dc.onJointBreakDel.Assign(methods);
                }

                // Calling if delegate exists
                if (dc.onJointBreakDel.Assigned)
                    dc.onJointBreakDel.Execute(breakForce);
            }
        }

        private void OnGUI() {
            CheckSetup();

            if (methodList != null)
            {
                // getting the list of Update methods
                Method[] methods = methodList.GetMethod(DelegateDefinitions.events.OnGUI);

                // Setting up if necessary
                if (!dc.onGUIDel.Assigned && methods.Length > 0)
                {
                    dc.onGUIDel.Assign(methods);
                }

                // Calling if delegate exists
                if (dc.onGUIDel.Assigned)
                    dc.onGUIDel.Execute();
            }
        }

        private void OnFailedToConnectToMasterServer(NetworkConnectionError info) {
            CheckSetup();

            if (methodList != null)
            {
                // getting the list of Update methods
                Method[] methods = methodList.GetMethod(DelegateDefinitions.events.OnFailedToConnectToMasterServer);

                // Setting up if necessary
                if (!dc.onFailedToConnectToMasterServerDel.Assigned && methods.Length > 0)
                {
                    dc.onFailedToConnectToMasterServerDel.Assign(methods);
                }

                // Calling if delegate exists
                if (dc.onFailedToConnectToMasterServerDel.Assigned)
                    dc.onFailedToConnectToMasterServerDel.Execute(info);
            }
        }

        private void OnFailedToConnect(NetworkConnectionError error) {
            CheckSetup();

            if (methodList != null)
            {
                // getting the list of Update methods
                Method[] methods = methodList.GetMethod(DelegateDefinitions.events.OnFailedToConnect);

                // Setting up if necessary
                if (!dc.onFailedToConnectType.Assigned && methods.Length > 0)
                {
                    dc.onFailedToConnectType.Assign(methods);
                }

                // Calling if delegate exists
                if (dc.onFailedToConnectType.Assigned)
                    dc.onFailedToConnectType.Execute(error);
            }
        }

        private void OnEnable() {
            CheckSetup();

            if (methodList != null)
            {
                // getting the list of Update methods
                Method[] methods = methodList.GetMethod(DelegateDefinitions.events.OnEnable);

                // Setting up if necessary
                if (!dc.onEnableDel.Assigned && methods.Length > 0)
                {
                    dc.onEnableDel.Assign(methods);
                }

                // Calling if delegate exists
                if (dc.onEnableDel.Assigned)
                    dc.onEnableDel.Execute();
            }
        }

        private void OnDrawGizmosSelected() {
            CheckSetup();

            if (methodList != null)
            {
                // getting the list of Update methods
                Method[] methods = methodList.GetMethod(DelegateDefinitions.events.OnDrawGizmosSelected);

                // Setting up if necessary
                if (!dc.onDrawGizmosSelectedDel.Assigned && methods.Length > 0)
                {
                    dc.onDrawGizmosSelectedDel.Assign(methods);
                }

                // Calling if delegate exists
                if (dc.onDrawGizmosSelectedDel.Assigned)
                    dc.onDrawGizmosSelectedDel.Execute();
            }
        }

        private void OnDrawGizmos() {
            CheckSetup();

            if (methodList != null)
            {
                // getting the list of Update methods
                Method[] methods = methodList.GetMethod(DelegateDefinitions.events.OnDrawGizmos);

                // Setting up if necessary
                if (!dc.onDrawGizmosDel.Assigned && methods.Length > 0)
                {
                    dc.onDrawGizmosDel.Assign(methods);
                }

                // Calling if delegate exists
                if (dc.onDrawGizmosDel.Assigned)
                    dc.onDrawGizmosDel.Execute();
            }
        }

        private void OnDisconnectedFromServer(NetworkDisconnection info) {
            CheckSetup();

            if (methodList != null)
            {
                // getting the list of Update methods
                Method[] methods = methodList.GetMethod(DelegateDefinitions.events.OnDisconnectedFromServer);

                // Setting up if necessary
                if (!dc.onDisconnectedFromServerDel.Assigned && methods.Length > 0)
                {
                    dc.onDisconnectedFromServerDel.Assign(methods);
                }

                // Calling if delegate exists
                if (dc.onDisconnectedFromServerDel.Assigned)
                    dc.onDisconnectedFromServerDel.Execute(info);
            }
        }

        private void OnDisable() {
            CheckSetup();

            if (methodList != null)
            {
                // getting the list of Update methods
                Method[] methods = methodList.GetMethod(DelegateDefinitions.events.OnDisable);

                // Setting up if necessary
                if (!dc.onDisableDel.Assigned && methods.Length > 0)
                {
                    dc.onDisableDel.Assign(methods);
                }

                // Calling if delegate exists
                if (dc.onDisableDel.Assigned)
                    dc.onDisableDel.Execute();
            }
        }

        private void OnDestroy() {
            CheckSetup();

            if (methodList != null)
            {
                // getting the list of Update methods
                Method[] methods = methodList.GetMethod(DelegateDefinitions.events.OnDestroy);

                // Setting up if necessary
                if (!dc.onDestroyDel.Assigned && methods.Length > 0)
                {
                    dc.onDestroyDel.Assign(methods);
                }

                // Calling if delegate exists
                if (dc.onDestroyDel.Assigned)
                    dc.onDestroyDel.Execute();
            }
        }

        private void OnControllerColliderHit(ControllerColliderHit hit) {
            CheckSetup();

            if (methodList != null)
            {
                // getting the list of Update methods
                Method[] methods = methodList.GetMethod(DelegateDefinitions.events.OnControllerColliderHit);

                // Setting up if necessary
                if (!dc.onControllerColliderHitType.Assigned && methods.Length > 0)
                {
                    dc.onControllerColliderHitType.Assign(methods);
                }

                // Calling if delegate exists
                if (dc.onControllerColliderHitType.Assigned)
                    dc.onControllerColliderHitType.Execute(hit);
            }
        }

        private void OnConnectedToServer() {
            CheckSetup();

            if (methodList != null)
            {
                // getting the list of Update methods
                Method[] methods = methodList.GetMethod(DelegateDefinitions.events.OnConnectedToServer);

                // Setting up if necessary
                if (!dc.onConnectedToServerDel.Assigned && methods.Length > 0)
                {
                    dc.onConnectedToServerDel.Assign(methods);
                }

                // Calling if delegate exists
                if (dc.onConnectedToServerDel.Assigned)
                    dc.onConnectedToServerDel.Execute();
            }
        }

        private void OnCollisionStay2D(Collision2D coll) {
            CheckSetup();

            if (methodList != null)
            {
                // getting the list of Update methods
                Method[] methods = methodList.GetMethod(DelegateDefinitions.events.OnCollisionStay2D);

                // Setting up if necessary
                if (!dc.onCollisionStay2DDel.Assigned && methods.Length > 0)
                {
                    dc.onCollisionStay2DDel.Assign(methods);
                }

                // Calling if delegate exists
                if (dc.onCollisionStay2DDel.Assigned)
                    dc.onCollisionStay2DDel.Execute(coll);
            }
        }

        private void OnCollisionStay(Collision collisionInfo) {
            CheckSetup();

            if (methodList != null)
            {
                // getting the list of Update methods
                Method[] methods = methodList.GetMethod(DelegateDefinitions.events.OnCollisionStay);

                // Setting up if necessary
                if (!dc.onCollisionStayDel.Assigned && methods.Length > 0)
                {
                    dc.onCollisionStayDel.Assign(methods);
                }

                // Calling if delegate exists
                if (dc.onCollisionStayDel.Assigned)
                    dc.onCollisionStayDel.Execute(collisionInfo);
            }
        }

        private void OnCollisionExit2D(Collision2D coll) {
            CheckSetup();

            if (methodList != null)
            {
                // getting the list of Update methods
                Method[] methods = methodList.GetMethod(DelegateDefinitions.events.OnCollisionExit2D);

                // Setting up if necessary
                if (!dc.onCollisionExit2DDel.Assigned && methods.Length > 0)
                {
                    dc.onCollisionExit2DDel.Assign(methods);
                }

                // Calling if delegate exists
                if (dc.onCollisionExit2DDel.Assigned)
                    dc.onCollisionExit2DDel.Execute(coll);
            }
        }

        private void OnCollisionExit(Collision collision) {
            CheckSetup();

            if (methodList != null)
            {
                // getting the list of Update methods
                Method[] methods = methodList.GetMethod(DelegateDefinitions.events.OnCollisionExit);

                // Setting up if necessary
                if (!dc.onCollisionExitDel.Assigned && methods.Length > 0)
                {
                    dc.onCollisionExitDel.Assign(methods);
                }

                // Calling if delegate exists
                if (dc.onCollisionExitDel.Assigned)
                    dc.onCollisionExitDel.Execute(collision);
            }
        }

        private void OnCollisionEnter2D(Collision2D coll) {
            CheckSetup();

            if (methodList != null)
            {
                // getting the list of Update methods
                Method[] methods = methodList.GetMethod(DelegateDefinitions.events.OnCollisionEnter2D);

                // Setting up if necessary
                if (!dc.onCollisionEnter2DDel.Assigned && methods.Length > 0)
                {
                    dc.onCollisionEnter2DDel.Assign(methods);
                }

                // Calling if delegate exists
                if (dc.onCollisionEnter2DDel.Assigned)
                    dc.onCollisionEnter2DDel.Execute(coll);
            }
        }

        private void OnCollisionEnter(Collision collision) {
            CheckSetup();

            if (methodList != null)
            {
                // getting the list of Update methods
                Method[] methods = methodList.GetMethod(DelegateDefinitions.events.OnCollisionEnter);

                // Setting up if necessary
                if (!dc.onCollisionEnterDel.Assigned && methods.Length > 0)
                {
                    dc.onCollisionEnterDel.Assign(methods);
                }

                // Calling if delegate exists
                if (dc.onCollisionEnterDel.Assigned)
                    dc.onCollisionEnterDel.Execute(collision);
            }
        }

        private void OnBecameVisible() {
            CheckSetup();

            if (methodList != null)
            {
                // getting the list of Update methods
                Method[] methods = methodList.GetMethod(DelegateDefinitions.events.OnBecameVisible);

                // Setting up if necessary
                if (!dc.onBecameVisibleDel.Assigned && methods.Length > 0)
                {
                    dc.onBecameVisibleDel.Assign(methods);
                }

                // Calling if delegate exists
                if (dc.onBecameVisibleDel.Assigned)
                    dc.onBecameVisibleDel.Execute();
            }
        }

        private void OnBecameInvisible() {
            CheckSetup();

            if (methodList != null)
            {
                // getting the list of Update methods
                Method[] methods = methodList.GetMethod(DelegateDefinitions.events.OnBecameInvisible);

                // Setting up if necessary
                if (!dc.onBecameInvisibleDel.Assigned && methods.Length > 0)
                {
                    dc.onBecameInvisibleDel.Assign(methods);
                }

                // Calling if delegate exists
                if (dc.onBecameInvisibleDel.Assigned)
                    dc.onBecameInvisibleDel.Execute();
            }
        }
        /*
        private void OnAudioFilterRead(int32 channels, float[] data) {
            //TODO Implement
        }*/

        private void OnApplicationQuit() {
            CheckSetup();

            if (methodList != null)
            {
                // getting the list of Update methods
                Method[] methods = methodList.GetMethod(DelegateDefinitions.events.OnApplicationQuit);

                // Setting up if necessary
                if (!dc.onApplicationQuitDel.Assigned && methods.Length > 0)
                {
                    dc.onApplicationQuitDel.Assign(methods);
                }

                // Calling if delegate exists
                if (dc.onApplicationQuitDel.Assigned)
                    dc.onApplicationQuitDel.Execute();
            }
        }

        private void OnApplicationPause(bool pause) {
            CheckSetup();

            if (methodList != null)
            {
                // getting the list of Update methods
                Method[] methods = methodList.GetMethod(DelegateDefinitions.events.OnApplicationPause);

                // Setting up if necessary
                if (!dc.onApplicationPauseDel.Assigned && methods.Length > 0)
                {
                    dc.onApplicationPauseDel.Assign(methods);
                }

                // Calling if delegate exists
                if (dc.onApplicationPauseDel.Assigned)
                    dc.onApplicationPauseDel.Execute(pause);
            }
        }

        private void OnApplicationFocus(bool focus) {
            CheckSetup();

            if (methodList != null)
            {
                // getting the list of Update methods
                Method[] methods = methodList.GetMethod(DelegateDefinitions.events.OnapplicationFocus);

                // Setting up if necessary
                if (!dc.onApplicationFocusDel.Assigned && methods.Length > 0)
                {
                    dc.onApplicationFocusDel.Assign(methods);
                }

                // Calling if delegate exists
                if (dc.onApplicationFocusDel.Assigned)
                    dc.onApplicationFocusDel.Execute(focus);
            }
        }

        private void OnAnimatorMove() {
            CheckSetup();

            if (methodList != null)
            {
                // getting the list of Update methods
                Method[] methods = methodList.GetMethod(DelegateDefinitions.events.OnAnimatorMove);

                // Setting up if necessary
                if (!dc.onAnimatorMoveDel.Assigned && methods.Length > 0)
                {
                    dc.onAnimatorMoveDel.Assign(methods);
                }

                // Calling if delegate exists
                if (dc.onAnimatorMoveDel.Assigned)
                    dc.onAnimatorMoveDel.Execute();
            }
        }

        private void OnAnimatorIK(int layerIndex) {
            CheckSetup();

            if (methodList != null)
            {
                // getting the list of Update methods
                Method[] methods = methodList.GetMethod(DelegateDefinitions.events.OnAnimatorIK);

                // Setting up if necessary
                if (!dc.onAnimatorIKDel.Assigned && methods.Length > 0)
                {
                    dc.onAnimatorIKDel.Assign(methods);
                }

                // Calling if delegate exists
                if (dc.onAnimatorIKDel.Assigned)
                    dc.onAnimatorIKDel.Execute(layerIndex);
            }
        }

        private void LateUpdate() {
            CheckSetup();

            if (methodList != null)
            {
                // getting the list of Update methods
                Method[] methods = methodList.GetMethod(DelegateDefinitions.events.LateUpdate);

                // Setting up if necessary
                if (!dc.lateUpdateDel.Assigned && methods.Length > 0)
                {
                    dc.lateUpdateDel.Assign(methods);
                }

                // Calling if delegate exists
                if (dc.lateUpdateDel.Assigned)
                    dc.lateUpdateDel.Execute();
            }
        }

        private void FixedUpdate() {
            CheckSetup();

            if (methodList != null)
            {
                // getting the list of Update methods
                Method[] methods = methodList.GetMethod(DelegateDefinitions.events.FixedUpdate);

                // Setting up if necessary
                if (!dc.fixedUpdateDel.Assigned && methods.Length > 0)
                {
                    dc.fixedUpdateDel.Assign(methods);
                }

                // Calling if delegate exists
                if (dc.fixedUpdateDel.Assigned)
                    dc.fixedUpdateDel.Execute();
            }
        }

        private void Awake() {
            CheckSetup();

            if (methodList != null)
            {
                // getting the list of Update methods
                Method[] methods = methodList.GetMethod(DelegateDefinitions.events.Awake);

                // Setting up if necessary
                if (!dc.awakeDel.Assigned && methods.Length > 0)
                {
                    dc.awakeDel.Assign(methods);
                }

                // Calling if delegate exists
                if (dc.awakeDel.Assigned)
                    dc.awakeDel.Execute();
            }
        }

        #endregion

        public void CheckSetup() {
            //if (dc == null || model == null || view == null || controller == null)
                //MakeReady();
        }
    }
}