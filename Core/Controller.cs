﻿// Copyright (c) 2015 ABXY Games, released under The MIT License
using UnityEngine;
using System.Collections;
using ABXY.OpenMVC.Helpers;

namespace ABXY.OpenMVC {

    /// <summary>
    /// Extend this class to create a controller. The Controller class is responsible for any logic related to
    /// the manipulation of data in the MVCMonobehaviour. The controller is only allowed access to the Model.
    /// Additionally, a Controller can only exist alongside a View, a Model, and the MVCMonobehaviour container. 
    /// 
    /// Note: The Unity callbacks in this class operate the same as the do in the standard Monobehaviour. As a result, 
    /// many method descriptions are pulled straight from Unity's documentation.
    /// </summary>
    /// <typeparam name="modelType">The type of the related model</typeparam>
    [System.Serializable]
    public class Controller <modelType> where modelType : Model{

        /// <summary>The MVCMonobehaviour that owns this controller</summary>
        private GenericMVCMonobehaviour owner;
        
        /// <summary>
        /// Returns the string representation of the monobehaviour
        /// </summary>
        /// <returns>The string representation </returns>
        public override string ToString() {
            return owner.ToString();
        }

        private modelType model_P;

        /// <summary>
        /// Sets up interoperability between the Controller class, its parent monobehavior, and it's related
        /// model and view. You'll never need to call this manually
        /// </summary>
        /// <param name="owner">The parent Monobehavior</param>
        /// <param name="model">The Controller's related model</param>
        public void Initialize(GenericMVCMonobehaviour owner, modelType model) {
            this.owner = owner;
            model_P = model;
            
        }

        /// <summary>Access point for the this MVCMonobehaviour's model</summary>
        protected modelType model {
            get {
                return model_P;
            }
        }

        # region Coroutine Functions

        /// <summary>
        /// Starts a coroutine.
        /// </summary>
        /// <param name="routine">Method definition</param>
        /// <example>
        /// The execution of a coroutine can be paused at any point using the yield statement. The yield 
        /// return value specifies when the coroutine is resumed. Coroutines are excellent when modelling 
        /// behaviour over several frames. Coroutines have virtually no performance overhead. 
        /// StartCoroutine function always returns immediately, however you can yield the result. 
        /// This will wait until the coroutine has finished execution. - From Unity3d.com documentation
        /// <code>
        /// using UnityEngine;
        /// using System.Collections;
        /// using ABXY.OpenMVC;
        /// 
        /// public class MyController :  Controller{
        ///
        ///     // Controller Setup:
        ///
        ///     MVC&lt;MyModel, MyView, MyController&gt; mvc;
        ///     public override void Setup(GenericMVC mvc) {
        ///         this.mvc = mvc.AsGeneric();
        ///     }
        ///     
        ///     Private void Start(){
        ///         StartCoroutine(WaitAndPrint(42f));
        ///     }
        ///     
        ///     private IEnumerator WaitAndPrint(float waitTime){
        ///         yield return new WaitForSeconds(waitTime);
        ///         Debug.Log("WaitAndPrint: " + waitTime);
        ///     }
        ///}
        /// </code>
        /// </example>
        public void StartCoroutine(IEnumerator routine) {
            owner.StartControllerCoroutine(routine);
        }

        /// <summary>
        /// Starts a coroutine named 'routine'
        /// </summary>
        /// <param name="routine">The routine to call</param>
        /// <example>
        /// The execution of a coroutine can be paused at any point using the yield statement. The yield 
        /// return value specifies when the coroutine is resumed. Coroutines are excellent when modelling 
        /// behaviour over several frames. Coroutines have virtually no performance overhead. 
        /// StartCoroutine function always returns immediately, however you can yield the result. 
        /// This will wait until the coroutine has finished execution. - From Unity3d.com documentation
        /// <code>
        /// using UnityEngine;
        /// using System.Collections;
        /// using ABXY.OpenMVC;
        /// 
        /// public class MyController :  Controller{
        ///
        ///     // Controller Setup:
        ///
        ///     MVC&lt;MyModel, MyView, MyController&gt; mvc;
        ///     public override void Setup(GenericMVC mvc) {
        ///         this.mvc = mvc.AsGeneric();
        ///     }
        ///     
        ///     Private void Start(){
        ///         StartCoroutine('WaitAndPrint');
        ///     }
        ///     
        ///     private IEnumerator WaitAndPrint(){
        ///         yield return new WaitForSeconds(42f);
        ///         Debug.Log("WaitAndPrint: " + 42f);
        ///     }
        ///}
        /// </code>
        /// </example>
        public void StartCoroutine(string routine) {
            owner.StartControllerCoroutine(routine);
        }

        /// <summary>
        /// Starts a coroutine named 'routine'
        /// </summary>
        /// <param name="routine">The routine to call</param>
        /// <param name="value">The argument to pass to the coroutine</param>
        /// /// <example>
        /// The execution of a coroutine can be paused at any point using the yield statement. The yield 
        /// return value specifies when the coroutine is resumed. Coroutines are excellent when modelling 
        /// behaviour over several frames. Coroutines have virtually no performance overhead. 
        /// StartCoroutine function always returns immediately, however you can yield the result. 
        /// This will wait until the coroutine has finished execution. - From Unity3d.com documentation
        /// <code>
        /// using UnityEngine;
        /// using System.Collections;
        /// using ABXY.OpenMVC;
        /// 
        /// public class MyController :  Controller{
        ///
        ///     // Controller Setup:
        ///
        ///     MVC&lt;MyModel, MyView, MyController&gt; mvc;
        ///     public override void Setup(GenericMVC mvc) {
        ///         this.mvc = mvc.AsGeneric();
        ///     }
        ///     
        ///     Private void Start(){
        ///         StartCoroutine('WaitAndPrint', 42f);
        ///     }
        ///     
        ///     private IEnumerator WaitAndPrint(float argument){
        ///         yield return new WaitForSeconds(argument);
        ///         Debug.Log("WaitAndPrint: " + argument);
        ///     }
        ///}
        /// </code>
        /// </example>
        public void StartCoroutine(string routine, object value) {
            owner.StartControllerCoroutine(routine, value);
        }


        #if UNITY_5_0
        public void StopCoroutine(IEnumerator routine) {
            owner.StopControllerCoroutine(routine);
        }
        #endif

        /// <summary>
        /// Stops the coroutine named by the caller. This only works with coroutines started using
        /// StartCoroutine(string routine)
        /// </summary>
        /// <param name="routine">The name of the coroutine to stop</param>
        public void StopCoroutine(string routine) {
            owner.StopControllerCoroutine(routine);
        }

        /// <summary>
        /// Stops all coroutines currently running on this MVCMonobehaviour
        /// </summary>
        public void StopAllCoroutines() {
            owner.StopAllControllerCoroutines();
        }

        #endregion


        /// <summary>
        /// This is called automatically when the controller is loaded into the MVC Monobehaviour.
        /// It hooks up this class so it can access the owning MVCMonobehaviour
        /// </summary>
        /// <param name="owner"></param>
        public void SetOwner(GenericMVCMonobehaviour owner) {
            this.owner = owner;
            if (owner == null){
                Debug.LogError("Context must not be null");
            }
        }

        #if (DOCUMENTATION)
        /// <summary>
        /// Update is called every frame, if the MVCMonoBehaviour is enabled.
        /// </summary>
        /// <example>
        /// <code language="csharp">
        /// <![CDATA[
        /// using UnityEngine;
        /// using System.Collections;
        /// using ABXY.OpenMVC;
        ///
        /// public class ExampleGameobject :  MVCMonobehaviour<ExampleModel,ExampleView,ExampleController>{}
        /// 
        /// public class ExampleModel: Model {
        ///     public float jumpForce = 4f;
        ///     
        ///     // Caching rigidbody, and exposing it to the View and Controller
        ///     private RigidBody rigidbody_P;
        ///     public Rigidbody rigidbody{
        ///         get{
        ///             if (rigidbody_P == null)
        ///                 rigidbody_p = (Rigidbody)GetComponent(typeof(Rigidbody));
        ///             return rigidbody_P
        ///         }
        ///     }
        /// }
        /// 
        /// public class ExampleView : View<ExampleController,ExampleModel>{}
        /// 
        /// public class ExampleController : Controller<ExampleModel>{
        ///     public void Update(){
        ///         if(Input.GetKeyDown(KeyCode.Space))
        ///             Jump();
        ///     }
        ///     
        ///     public void Jump(){
        ///         rigidbody.AddForce(new Vector3(0f,model.jumpForce,0f));
        ///     }
        /// }
        /// ]]>
        /// </code>
        /// </example>
        public void Update() {
            
        }

        /// <summary>
        /// Start is called on the frame when a script is enabled just before any of the Update methods is called the first time.
        /// </summary>
        /// <example>
        /// <code language="csharp">
        /// <![CDATA[
        /// using UnityEngine;
        /// using System.Collections;
        /// using ABXY.OpenMVC;
        ///
        /// public class ExampleGameobject :  MVCMonobehaviour<ExampleModel,ExampleView,ExampleController>{}
        /// 
        /// public class ExampleModel: Model {}
        /// 
        /// public class ExampleView : View<ExampleController,ExampleModel>{}
        /// 
        /// public class ExampleController : Controller<ExampleModel>{
        ///     public void Start(){
        ///         Debug.log("This script has been enabled");
        ///     }
        /// }
        /// ]]>
        /// </code>
        /// </example>  
        public void Start() {

        }

        /// <summary>
        /// Reset to default values.
        /// </summary>
        /// <example>
        /// <code language="csharp">
        /// <![CDATA[
        /// using UnityEngine;
        /// using System.Collections;
        /// using ABXY.OpenMVC;
        ///
        /// public class ExampleGameobject :  MVCMonobehaviour<ExampleModel,ExampleView,ExampleController>{}
        /// 
        /// public class ExampleModel: Model {}
        /// 
        /// public class ExampleView : View<ExampleController,ExampleModel>{}
        /// 
        /// public class ExampleController : Controller<ExampleModel>{
        ///     public void Reset(){
        ///         Debug.Log("This object has been reset");
        ///     }
        /// }
        /// ]]>
        /// </code>
        /// </example>  
        public void Reset() {

        }

        /// <summary>
        /// This function is called when the script is loaded or a value is changed in the inspector (Called in the editor only).
        /// </summary>
        /// <example>
        /// <code language="csharp">
        /// <![CDATA[
        /// using UnityEngine;
        /// using System.Collections;
        /// using ABXY.OpenMVC;
        ///
        /// public class ExampleGameobject :  MVCMonobehaviour<ExampleModel,ExampleView,ExampleController>{}
        /// 
        /// public class ExampleModel: Model {}
        /// 
        /// public class ExampleView : View<ExampleController,ExampleModel>{}
        /// 
        /// public class ExampleController : Controller<ExampleModel>{
        ///     public void OnValidate(){
        ///         Debug.Log("This object has been deserialized");
        ///     }
        /// }
        /// ]]>
        /// </code>
        /// </example>  
        public void OnValidate() {

        }

        /// <summary>
        /// Sent each frame where another object is within a trigger collider attached to this object (2D physics only).
        /// </summary>
        /// <param name="other">The Collider2D on the intruding object</param>
        /// <example>
        /// <code language="csharp">
        /// <![CDATA[
        /// using UnityEngine;
        /// using System.Collections;
        /// using ABXY.OpenMVC;
        ///
        /// public class ExampleGameobject :  MVCMonobehaviour<ExampleModel,ExampleView,ExampleController>{}
        /// 
        /// public class ExampleModel: Model {}
        /// 
        /// public class ExampleView : View<ExampleController,ExampleModel>{}
        /// 
        /// public class ExampleController : Controller<ExampleModel>{
        ///     public void OnTriggerStay2D(Collider2D other){
        ///         Debug.log("Colliding with " + other.name);
        ///     }
        /// }
        /// ]]>
        /// </code>
        /// </example> 
        public void OnTriggerStay2D(Collider2D other) {
            
        }

        /// <summary>
        /// OnTriggerStay is called once per frame for every Collider other that is touching the trigger.
        /// </summary>
        /// <param name="other">The Collider of the intruding object</param>
        /// <example>
        /// <code language="csharp">
        /// <![CDATA[
        /// using UnityEngine;
        /// using System.Collections;
        /// using ABXY.OpenMVC;
        ///
        /// public class ExampleGameobject :  MVCMonobehaviour<ExampleModel,ExampleView,ExampleController>{}
        /// 
        /// public class ExampleModel: Model {}
        /// 
        /// public class ExampleView : View<ExampleController,ExampleModel>{}
        /// 
        /// public class ExampleController : Controller<ExampleModel>{
        ///     public void OnTriggerStay(Collider other){
        ///         Debug.log("Colliding with " + other.name);
        ///     }
        /// }
        /// ]]>
        /// </code>
        /// </example> 
        public void OnTriggerStay(Collider other) {

        }

        /// <summary>
        /// Sent when another object leaves a trigger collider attached to this object (2D physics only).
        /// </summary>
        /// <param name="other">The Collider2D of the exiting object</param>
        /// <example>
        /// <code language="csharp">
        /// <![CDATA[
        /// using UnityEngine;
        /// using System.Collections;
        /// using ABXY.OpenMVC;
        ///
        /// public class ExampleGameobject :  MVCMonobehaviour<ExampleModel,ExampleView,ExampleController>{}
        /// 
        /// public class ExampleModel: Model {}
        /// 
        /// public class ExampleView : View<ExampleController,ExampleModel>{}
        /// 
        /// public class ExampleController : Controller<ExampleModel>{
        ///     public void OnTriggerExit2D(Collider2D other){
        ///         Debug.log("Collider " + other.name + " has exited this collider");
        ///     }
        /// }
        /// ]]>
        /// </code>
        /// </example> 
        public void OnTriggerExit2D(Collider2D other) {

        }

        /// <summary>
        /// OnTriggerExit is called when the Collider other has stopped touching the trigger.
        /// </summary>
        /// <param name="other">The collider of the exiting object</param>
        /// <example>
        /// <code language="csharp">
        /// <![CDATA[
        /// using UnityEngine;
        /// using System.Collections;
        /// using ABXY.OpenMVC;
        ///
        /// public class ExampleGameobject :  MVCMonobehaviour<ExampleModel,ExampleView,ExampleController>{}
        /// 
        /// public class ExampleModel: Model {}
        /// 
        /// public class ExampleView : View<ExampleController,ExampleModel>{}
        /// 
        /// public class ExampleController : Controller<ExampleModel>{
        ///     public void OnTriggerExit(Collider other){
        ///         Debug.log("Collider " + other.name + " has exited this collider");
        ///     }
        /// }
        /// ]]>
        /// </code>
        /// </example> 
        public void OnTriggerExit(Collider other) {

        }

        /// <summary>
        /// Sent when another object enters a trigger collider attached to this object (2D physics only).
        /// </summary>
        /// <param name="other">The Collider2D of the entering object</param>
        /// <example>
        /// <code language="csharp">
        /// <![CDATA[
        /// using UnityEngine;
        /// using System.Collections;
        /// using ABXY.OpenMVC;
        ///
        /// public class ExampleGameobject :  MVCMonobehaviour<ExampleModel,ExampleView,ExampleController>{}
        /// 
        /// public class ExampleModel: Model {}
        /// 
        /// public class ExampleView : View<ExampleController,ExampleModel>{}
        /// 
        /// public class ExampleController : Controller<ExampleModel>{
        ///     public void OnTriggerEnter2D(Collider2D other){
        ///         Debug.log("Collider " + other.name + " has entered this collider");
        ///     }
        /// }
        /// ]]>
        /// </code>
        /// </example> 
        public void OnTriggerEnter2D(Collider2D other) {

        }

        /// <summary>
        /// OnTriggerEnter is called when the Collider other enters the trigger.
        /// </summary>
        /// <param name="other">The Collider of the entering object</param>
        /// <example>
        /// <code language="csharp">
        /// <![CDATA[
        /// using UnityEngine;
        /// using System.Collections;
        /// using ABXY.OpenMVC;
        ///
        /// public class ExampleGameobject :  MVCMonobehaviour<ExampleModel,ExampleView,ExampleController>{}
        /// 
        /// public class ExampleModel: Model {}
        /// 
        /// public class ExampleView : View<ExampleController,ExampleModel>{}
        /// 
        /// public class ExampleController : Controller<ExampleModel>{
        ///     public void OnTriggerEnter(Collider other){
        ///         Debug.log("Collider " + other.name + " has entered this collider");
        ///     }
        /// }
        /// ]]>
        /// </code>
        /// </example> 
        public void OnTriggerEnter(Collider other) {

        }

         /// <summary>
        /// Called on the server whenever a Network.InitializeServer was invoked and has completed.
        /// </summary>
        public void OnServerInitialized() {

        }

        /// <summary>
        /// Used to customize synchronization of variables in a script watched by a network view.
        /// </summary>
        /// <param name="info"></param>
        /// <param name="stream"></param>
        public void OnSerializeNetworkView(NetworkMessageInfo info, BitStream stream) {

        }
        /// <summary>
        /// Called on the server whenever a new player has successfully connected.
        /// </summary>
        /// <param name="player">The connected Player</param>
        public void OnPlayerConnected(NetworkPlayer player) {

        }

        /// <summary>
        /// OnParticleCollision is called when a particle hits a collider.
        /// </summary>
        /// <param name="other">The gameobject representing the particle system, or the collider</param>
        public void OnParticleCollision(GameObject other) {

        }

        /// <summary>
        /// Called on objects which have been network instantiated with Network.Instantiate.
        /// </summary>
        /// <param name="info"></param>
        public void OnNetworkInstantiate(NetworkMessageInfo info) {

        }

        /// <summary>
        /// OnMouseUpAsButton is only called when the mouse is released over the same GUIElement or Collider as it was pressed.
        /// </summary>
        public void OnMouseUpAsButton() {

        }

        /// <summary>
        /// OnMouseUp is called when the user has released the mouse button.
        /// </summary>
        public void OnMouseUp() {

        }

        /// <summary>
        /// Called every frame while the mouse is over the GUIElement or Collider.
        /// </summary>
        public void OnMouseOver() {

        }

        /// <summary>
        /// Called when the mouse is not any longer over the GUIElement or Collider.
        /// </summary>
        public void OnMouseExit() {

        }

        /// <summary>
        /// Called when the mouse enters the GUIElement or Collider.
        /// </summary>
        public void OnMouseEnter() {

        }

        /// <summary>
        /// OnMouseDrag is called when the user has clicked on a GUIElement or Collider and is still holding down the mouse.
        /// </summary>
        public void OnMouseDrag() {

        }

        /// <summary>
        /// OnMouseDown is called when the user has pressed the mouse button while over the GUIElement or Collider.
        /// </summary>
        public void OnMouseDown() {

        }

        /// <summary>
        /// Called on clients or servers when reporting events from the MasterServer.
        /// </summary>
        /// <param name="msEvent"></param>
        public void OnMasterServerEvent(MasterServerEvent msEvent) {

        }

        /// <summary>
        /// This function is called after a new level was loaded.
        /// </summary>
        /// <param name="level">The level number</param>
        public void OnLevelWasLoaded(int level) {

        }

        /// <summary>
        /// Called when a joint attached to the same game object broke.
        /// </summary>
        /// <param name="breakForce">The break force that was applied to the joint</param>
        public void OnJointBreak(float breakForce) {

        }

         /// <summary>
        /// Called on the client when the connection was lost or you disconnected from the server.
        /// </summary>
        /// <param name="info"></param>
        public void OnDisconnectedFromServer(NetworkDisconnection info) {

        }

        /// <summary>
        /// This function is called when the behaviour becomes disabled () or inactive.
        /// </summary>/// <example>
        /// <code language="csharp">
        /// <![CDATA[
        /// using UnityEngine;
        /// using System.Collections;
        /// using ABXY.OpenMVC;
        ///
        /// public class ExampleGameobject :  MVCMonobehaviour<ExampleModel,ExampleView,ExampleController>{}
        /// 
        /// public class ExampleModel: Model {}
        /// 
        /// public class ExampleView : View<ExampleController,ExampleModel>{}
        /// 
        /// public class ExampleController : Controller<ExampleModel>{
        ///     public void OnDisable(){
        ///         Debug.Log("This object has been disabled");
        ///     }
        /// }
        /// ]]>
        /// </code>
        /// </example>  
        public void OnDisable() {

        }

        /// <summary>
        /// This function is called when the MonoBehaviour will be destroyed.
        /// </summary>
        /// <example>
        /// <code language="csharp">
        /// <![CDATA[
        /// using UnityEngine;
        /// using System.Collections;
        /// using ABXY.OpenMVC;
        ///
        /// public class ExampleGameobject :  MVCMonobehaviour<ExampleModel,ExampleView,ExampleController>{}
        /// 
        /// public class ExampleModel: Model {}
        /// 
        /// public class ExampleView : View<ExampleController,ExampleModel>{}
        /// 
        /// public class ExampleController : Controller<ExampleModel>{
        ///     public void OnDestroy(){
        ///         Debug.Log("This object is being destroyed");
        ///     }
        /// }
        /// ]]>
        /// </code>
        /// </example>  
        public void OnDestroy() {

        }

        /// <summary>
        /// OnControllerColliderHit is called when the controller hits a collider while performing a Move.
        /// </summary>
        /// <param name="hit"></param>
        public void OnControllerColliderHit(ControllerColliderHit hit) {

        }

        /// <summary>
        /// Called on the client when you have successfully connected to a server.
        /// </summary>
        public void OnConnectedToServer() {

        }

        /// <summary>
        /// Sent each frame where a collider on another object is touching this object's collider (2D physics only).
        /// </summary>
        /// <param name="coll">The collision info for this interaction</param>
        public void OnCollisionStay2D(Collision2D coll) {

        }

        /// <summary>
        /// OnCollisionStay is called once per frame for every collider/rigidbody that is touching rigidbody/collider.
        /// </summary>
        /// <param name="collisionInfo"> The collision info for this interaction</param>
        public void OnCollisionStay(Collision collisionInfo) {

        }

        /// <summary>
        /// Sent when a collider on another object stops touching this object's collider (2D physics only).
        /// </summary>
        /// <param name="coll">The collision info for this interaction</param>
        public void OnCollisionExit2D(Collision2D coll) {

        }

        /// <summary>
        /// OnCollisionExit is called when this collider/rigidbody has stopped touching another rigidbody/collider.
        /// </summary>
        /// <param name="collision">The collision info for this interaction</param>
        public void OnCollisionExit(Collision collision) {

        }

        /// <summary>
        /// Sent when an incoming collider makes contact with this object's collider (2D physics only).
        /// </summary>
        /// <param name="coll">The collision info for this interaction</param>
        public void OnCollisionEnter2D(Collision2D coll) {

        }

        /// <summary>
        /// OnCollisionEnter is called when this collider/rigidbody has begun touching another rigidbody/collider.
        /// </summary>
        /// <param name="collision">The collision info for this interaction</param>
        public void OnCollisionEnter(Collision collision) {

        }

        /// <summary>
        /// OnBecameVisible is called when the renderer became visible by any camera.
        /// </summary>
        public void OnBecameVisible() {

        }

        /// <summary>
        /// OnBecameInvisible is called when the renderer is no longer visible by any camera.
        /// </summary>
        public void OnBecameInvisible() {

        }

        /// <summary>
        /// If OnAudioFilterRead is implemented, Unity will insert a custom filter into the audio DSP chain.
        /// </summary>
        /// <param name="channels"></param>
        /// <param name="data"></param>
        public void OnAudioFilterRead(int channels, float[] data) {

        }

        /// <summary>
        /// Sent to all game objects before the application is quit.
        /// </summary>
        public void OnApplicationQuit() {

        }

        /// <summary>
        /// Sent to all game objects when the player pauses.
        /// </summary>
        /// <param name="pause">True for unpaused, false for paused</param>
        public void OnApplicationPause(bool pause) {

        }

        /// <summary>
        /// Sent to all game objects when the player gets or loses focus.
        /// </summary>
        /// <param name="focus">True when focused, false when unfocused</param>
        public void OnApplicationFocus(bool focus) {

        }

        /// <summary>
        /// Callback for processing animation movements for modifying root motion.
        /// </summary>
        public void OnAnimatorMove() {

        }

        /// <summary>
        /// Callback for setting up animation IK (inverse kinematics).
        /// </summary>
        /// <param name="layerIndex"></param>
        public void OnAnimatorIK(int layerIndex) {

        }

        /// <summary>
        /// LateUpdate is called every frame, if the Behaviour is enabled.
        /// </summary>
        /// 
        public void LateUpdate() {

        }

        /// <summary>
        /// This function is called every fixed framerate frame, if the MonoBehaviour is enabled.
        /// </summary>
        public void FixedUpdate() {

        }

        /// <summary>
        /// Awake is called when the script instance is being loaded.
        /// </summary>
        public void Awake() {

        }

        /// <summary>
        /// OnRenderObject is called after camera has rendered the scene.
        /// </summary>
        public void OnRenderObject() {

        }

        /// <summary>
        /// OnRenderImage is called after all rendering is complete to render image.
        /// </summary>
        /// <param name="destination">outgoing image</param>
        /// <param name="source">incoming image</param>
        public void OnRenderImage(RenderTexture destination, RenderTexture source) {

        }

        /// <summary>
        /// OnPreRender is called before a camera starts rendering the scene.
        /// </summary>
        public void OnPreRender() {

        }

        /// <summary>
        /// OnPreCull is called before a camera culls the scene.
        /// </summary>
        public void OnPreCull() {

        }

        /// <summary>
        /// OnPostRender is called after a camera finished rendering the scene.
        /// </summary>
        public void OnPostRender() {

        }
        #endif
    }
}
