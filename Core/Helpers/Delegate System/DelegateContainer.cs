﻿// Copyright (c) 2015 ABXY Games, released under The MIT License
using ABXY.OpenMVC.DelegateTypes;
using ABXY.OpenMVC.Helpers.DelegateTypes;

namespace ABXY.OpenMVC.Helpers.DelegateSystem
{

    /// <summary>
    /// Container object for Unity event delegates. Discard and re-instance this object to reset
    /// </summary>
    public class DelegateContainer{
        //Event Delegates
        #region delegates
        public GenericDelegate updateDel = new GenericDelegate();
        public GenericDelegate startDel = new GenericDelegate();
        public GenericDelegate resetDel = new GenericDelegate();
        public GenericDelegate onWillRenderObjectDel = new GenericDelegate();
        public GenericDelegate onValidateDel = new GenericDelegate();
        public Collider2DParamDelegate onTriggerStay2DDel = new Collider2DParamDelegate();
        public ColliderParamDelegate onTriggerStayDel = new ColliderParamDelegate();
        public Collider2DParamDelegate onTriggerExit2D = new Collider2DParamDelegate();
        public ColliderParamDelegate onTriggerExitDel = new ColliderParamDelegate();
        public Collider2DParamDelegate onTriggerEnter2DDel = new Collider2DParamDelegate();
        public ColliderParamDelegate onTriggerEnterDel = new ColliderParamDelegate();
        public GenericDelegate onServerInitializedDel = new GenericDelegate();
        public GenericDelegate onSerializeNetworkView = new GenericDelegate();
        public GenericDelegate OnRenderObjectDel = new GenericDelegate();
        public OnRenderImageDelegate onRenderImageDel = new OnRenderImageDelegate();
        public GenericDelegate onPreRenderTypeDel = new GenericDelegate();
        public GenericDelegate onPreCullDel = new GenericDelegate();
        public GenericDelegate onPostRenderDel = new GenericDelegate();
        public NetworkPlayerParamDelegate onPlayerDisconnectedDel = new NetworkPlayerParamDelegate();
        public NetworkPlayerParamDelegate onPlayerConnectedDel = new NetworkPlayerParamDelegate();
        public GameObjectParamDelegate onParticleCollisionDel = new GameObjectParamDelegate();
        public NetworkMessageInfoParamDel onNetworkInstantiateDel = new NetworkMessageInfoParamDel();
        public GenericDelegate onMouseUpAsButtonDel = new GenericDelegate();
        public GenericDelegate onMouseUpDel = new GenericDelegate();
        public GenericDelegate onMouseOverDel = new GenericDelegate();
        public GenericDelegate onMouseExitDel = new GenericDelegate();
        public GenericDelegate onMouseEnterDel = new GenericDelegate();
        public GenericDelegate onMouseDragDel = new GenericDelegate();
        public GenericDelegate onMouseDownDel = new GenericDelegate();
        public MasterServerEventParamDelegate onMasterServerEvent = new MasterServerEventParamDelegate();
        public IntParamDelegate onLevelWasLoadedDel = new IntParamDelegate();
        public FloatParamDelegate onJointBreakDel = new FloatParamDelegate();
        public GenericDelegate onGUIDel = new GenericDelegate();
        public NetworkConnectionErrorParamDelegate onFailedToConnectToMasterServerDel = new NetworkConnectionErrorParamDelegate();
        public NetworkConnectionErrorParamDelegate onFailedToConnectType = new NetworkConnectionErrorParamDelegate();
        public GenericDelegate onEnableDel = new GenericDelegate();
        public GenericDelegate onDrawGizmosSelectedDel = new GenericDelegate();
        public GenericDelegate onDrawGizmosDel = new GenericDelegate();
        public NetworkDisconnectionParamDelegate onDisconnectedFromServerDel = new NetworkDisconnectionParamDelegate();
        public GenericDelegate onDisableDel = new GenericDelegate();
        public GenericDelegate onDestroyDel = new GenericDelegate();
        public ControllerColliderHitParamDelegate onControllerColliderHitType = new ControllerColliderHitParamDelegate();
        public GenericDelegate onConnectedToServerDel = new GenericDelegate();
        public Collision2DParamDelegate onCollisionStay2DDel = new Collision2DParamDelegate();
        public CollisionParamDelegate onCollisionStayDel = new CollisionParamDelegate();
        public Collision2DParamDelegate onCollisionExit2DDel = new Collision2DParamDelegate();
        public CollisionParamDelegate onCollisionExitDel = new CollisionParamDelegate();
        public Collision2DParamDelegate onCollisionEnter2DDel = new Collision2DParamDelegate();
        public CollisionParamDelegate onCollisionEnterDel = new CollisionParamDelegate();
        public GenericDelegate onBecameVisibleDel = new GenericDelegate();
        public GenericDelegate onBecameInvisibleDel = new GenericDelegate();
        public GenericDelegate onAudioFilterReadDel = new GenericDelegate();
        public GenericDelegate onApplicationQuitDel = new GenericDelegate();
        public BoolParamDelegate onApplicationPauseDel = new BoolParamDelegate();
        public BoolParamDelegate onApplicationFocusDel = new BoolParamDelegate();
        public GenericDelegate onAnimatorMoveDel = new GenericDelegate();
        public IntParamDelegate onAnimatorIKDel = new IntParamDelegate();
        public GenericDelegate lateUpdateDel = new GenericDelegate();
        public GenericDelegate fixedUpdateDel = new GenericDelegate();
        public GenericDelegate awakeDel = new GenericDelegate();

        #endregion
    }
}
