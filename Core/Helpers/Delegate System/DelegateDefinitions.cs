﻿// Copyright (c) 2015 ABXY Games, released under The MIT License
namespace ABXY.OpenMVC.Helpers.DelegateSystem {

    /// <summary>
    /// Class defining the Unity events available in the Model, View, and Controller classes
    /// </summary>
    public class DelegateDefinitions {

        //TODO order and binary search these
        /// <summary>
        /// Definition for the Unity events available in the Controller
        /// </summary>
        private static events[] controllerDelegates_p = new events[]{
            events.Update,
            events.Start,
            events.Reset,
            events.OnValidate,
            events.OnTriggerStay2D,
            events.OnTriggerStay,
            events.OnTriggerExit2D,
            events.OnTriggerExit,
            events.OnTriggerEnter2D,
            events.OnTriggerEnter,
            events.OnServerInitialized,
            events.OnSerializeNetworkView,
            events.OnPlayerDisconnected,
            events.OnPlayerConnected,
            events.OnParticleCollision,
            events.OnNetworkInstantiate,
            events.OnMouseUpAsButton,
            events.OnMouseUp,
            events.OnMouseOver,
            events.OnMouseExit,
            events.OnMouseEnter,
            events.OnMouseDrag,
            events.OnMouseDown,
            events.OnMasterServerEvent,
            events.OnLevelWasLoaded,
            events.OnJointBreak,
            events.OnFailedToConnectToMasterServer,
            events.OnFailedToConnect,
            events.OnEnable,
            events.OnDisconnectedFromServer,
            events.OnDisable,
            events.OnDestroy,
            events.OnControllerColliderHit,
            events.OnConnectedToServer,
            events.OnCollisionStay2D,
            events.OnCollisionStay,
            events.OnCollisionExit2D,
            events.OnCollisionExit,
            events.OnCollisionEnter2D,
            events.OnCollisionEnter,
            events.OnBecameVisible,
            events.OnBecameInvisible,
            events.OnAudioFilterRead,
            events.OnApplicationQuit,
            events.OnApplicationPause,
            events.OnapplicationFocus,
            events.OnAnimatorMove,
            events.OnAnimatorIK,
            events.LateUpdate,
            events.FixedUpdate,
            events.Awake,
            events.OnRenderObject,
            events.OnRenderImage,
            events.OnPreRender,
            events.OnPreCull,
            events.OnPostRender,
        };

        /// <summary>
        /// Definition for the Unity events available in the Model
        /// </summary>
        private static events[] modelDelegates_p = new events[]{
            events.Start,
            events.Reset,
            events.OnValidate,
            events.OnEnable,
            events.OnDisable,
            events.OnDestroy,
        };


        /// <summary>
        /// Definition for the Unity events available in the View
        /// </summary>
        private static events[] viewDelegates_p = new events[]{
            events.Start,
            events.Reset,
            events.OnWillRenderObject,
            events.OnValidate,
            events.OnGUI,
            events.OnEnable,
            events.OnDrawGizmosSelected,
            events.OnDrawGizmos,
        };

        //Delegate Definition Getters
        /// <summary>
        /// Protected accessor for the Unity events available to Controllers
        /// </summary>
        public static events[] controllerDelegates {
            get {
                return controllerDelegates_p;
            }
        }

        /// <summary>
        /// Protected accessor for the Unity events available to Models
        /// </summary>
        public static events[] modelDelegates {
            get {
                return modelDelegates_p;
            }
        }


        /// <summary>
        /// Protected accessor for the Unity events available to View
        /// </summary>
        public static events[] viewDelegates {
            get {
                return viewDelegates_p;
            }
        }


        /// <summary>
        /// Enumeration of all of the Unity events available to MVC objects
        /// </summary>
        public enum events {
            Update,
            Start,
            Reset,
            OnWillRenderObject,
            OnValidate,
            OnTriggerStay2D,
            OnTriggerStay,
            OnTriggerExit2D,
            OnTriggerExit,
            OnTriggerEnter2D,
            OnTriggerEnter,
            OnServerInitialized,
            OnSerializeNetworkView,
            OnRenderObject,
            OnRenderImage,
            OnPreRender,
            OnPreCull,
            OnPostRender,
            OnPlayerDisconnected,
            OnPlayerConnected,
            OnParticleCollision,
            OnNetworkInstantiate,
            OnMouseUpAsButton,
            OnMouseUp,
            OnMouseOver,
            OnMouseExit,
            OnMouseEnter,
            OnMouseDrag,
            OnMouseDown,
            OnMasterServerEvent,
            OnLevelWasLoaded,
            OnJointBreak,
            OnGUI,
            OnFailedToConnectToMasterServer,
            OnFailedToConnect,
            OnEnable,
            OnDrawGizmosSelected,
            OnDrawGizmos,
            OnDisconnectedFromServer,
            OnDisable,
            OnDestroy,
            OnControllerColliderHit,
            OnConnectedToServer,
            OnCollisionStay2D,
            OnCollisionStay,
            OnCollisionExit2D,
            OnCollisionExit,
            OnCollisionEnter2D,
            OnCollisionEnter,
            OnBecameVisible,
            OnBecameInvisible,
            OnAudioFilterRead,
            OnApplicationQuit,
            OnApplicationPause,
            OnapplicationFocus,
            OnAnimatorMove,
            OnAnimatorIK,
            LateUpdate,
            FixedUpdate,
            Awake,
            NotADelegate
        }


        /// <summary>
        /// Enumeration allowing objects to identify whether they belong to a Model, a View, or Controller
        /// </summary>
        public enum MemberOf{
            model,
            view,
            control
        }
        
        /// <summary>
        /// Checks if the given event name is allowed to be attached to a Model, View, or Controller
        /// </summary>
        /// <param name="belongsTo">Specify to check whether the given event name belongs in a model, view, or controller</param>
        /// <param name="eventToCheck">The name of the event to check</param>
        /// <returns>Returns true if the named event is allowed in the given type, false if it is not.</returns>
        public static bool IsADelegateOf(MemberOf belongsTo, string eventToCheck) {
            //string eventString = System.Enum.GetName(typeof(DelegateDefinitions.events), eventToCheck);
            
            events[] eventsToCheck = new events[]{};
            if (belongsTo == MemberOf.model)
                eventsToCheck = modelDelegates_p;
            else if (belongsTo == MemberOf.view)
                eventsToCheck = viewDelegates_p;
            else
                eventsToCheck = controllerDelegates;
            foreach (events eventName in eventsToCheck) {
                if (eventName.ToString().ToLower() == eventToCheck.ToLower())
                {
                    
                    return true;
                }
            }
            return false;
        }


    }
}
