﻿using UnityEngine;
using System.Collections;
using VexeEmbedded.Runtime.Types;

namespace ABXY.OpenMVC.Helpers {

    /// <summary>
    /// Generic base for the MVCMonobehaviour class. Allows for common interchange in the Model, 
    /// View, and Controller classes
    /// </summary>
    public class GenericMVCMonobehaviour : BetterBehaviour {
        

        /// <summary>
        /// Accessor function so that Controllers can start Coroutines
        /// </summary>
        /// <param name="routine">The Coroutine to call</param>
        public void StartControllerCoroutine(IEnumerator routine) {
            StartCoroutine(routine);
        }


        /// <summary>
        /// Accessor function so that Controllers can start Coroutines
        /// </summary>
        /// <param name="routine">The name of the Coroutine to call</param>
        public void StartControllerCoroutine(string routine) {
            StartCoroutine(routine);
        }


        /// <summary>
        /// Accessor function so that Controllers can start Coroutines
        /// </summary>
        /// <param name="routine">The name of the Coroutine to call</param>
        /// <param name="value">Argument for the Coroutine</param>
        public void StartControllerCoroutine(string routine, object value) {
            StartCoroutine(routine, value);
        }


        //TODO enable:
        #if UNITY_5_0
        public void StopControllerCoroutine(IEnumerator routine) {
            StopCoroutine(routine);
        }
        #endif


        /// <summary>
        /// Accessor function for stopping the named coroutine
        /// </summary>
        /// <param name="routine">The name of the Coroutine to stop</param>
        public void StopControllerCoroutine(string routine) {
            StopCoroutine(routine);
        }


        /// <summary>
        /// Accessor function for stopping all coroutines
        /// </summary>
        public void StopAllControllerCoroutines() {
            StopAllCoroutines();
        }

    }
}