﻿using UnityEngine;
using ABXY.OpenMVC.Helpers.ChangeSubscription;
using System.Collections.Generic;

namespace ABXY.OpenMVC.Helpers.ChangeSubscription{


    /// <summary>
    /// This class handles storage and updates for subscribers to variable changes. This functionality is built
    /// into OpenMVC's Models using this class, but may be used outside of this class as well. Just instantiate
    /// ChangeSubscriptionHandler and call RefreshSubscribers() every frame.
    /// </summary>
    public class ChangeSubscriptionHandler {


        /// <summary>Change subscribers to variables in this model</summary>
        private Dictionary<string, Subscriber> subscribers = new Dictionary<string, Subscriber>();


        /// <summary>
        /// Gets the number of subscribers managed by this handler
        /// </summary>
        public int getChangeSubscriberCount {
            get {
                int count = 0;
                foreach (KeyValuePair<string, Subscriber> sub in subscribers) {
                    count += sub.Value.subscriberCount;
                }
                return count;
            }
        }


        /// <summary>
        /// Subscribes the specified delegate to changes of the specified delegate
        /// </summary>
        /// <param name="varName">Name of the variable to subscribe to</param>
        /// <param name="subscriber">This delegate will be called when the specified variable
        /// is changed</param>
        public void AddChangeSubscriber(string varName, Subscriber.ChangeSubscriber subscriber) {
            if (subscribers.ContainsKey(varName)) {
                subscribers[varName].AddSubscriber(subscriber);
            }
            else {
                Subscriber newSub = new Subscriber(varName);
                newSub.AddSubscriber(subscriber);
                subscribers.Add(varName, newSub);
            }
        }


        /// <summary>
        /// Unsubscribes the specified delegate to changes of the specified variables
        /// </summary>
        /// <param name="varName">Name of the variable to unsubscribe to</param>
        /// <param name="subscriber">This delegate will no longer be called</param>
        public void RemoveChangeSubscriber(string varName, Subscriber.ChangeSubscriber subscriber) {
            if (subscribers.ContainsKey(varName)) {
                subscribers[varName].RemoveSubscriber(subscriber);
            }
            else {
                Debug.LogError(varName + " is not a member and thus cannot be "
                    + "subscribed to. Check that you are calling subscribe on the right model, and that "
                + "your variable name is spelled correctly");
            }
        }

        

        /// <summary>
        /// Updates other classes that have registered for variable changes. In practice, you will never
        /// need to call this, it is called automatically every frame.
        /// </summary>
        public void RefreshSubscribers(Dictionary<string, object> variables) {
            foreach (KeyValuePair<string, object> variable in variables) {
                if (subscribers.ContainsKey(variable.Key)) {
                    subscribers[variable.Key].WatchedObject = variable.Value;
                }
            }
        }


        /// <summary>
        /// Clears out all subscribers to this model
        /// </summary>
        public void RemoveSubscribers() {
            subscribers.Clear();
        }
    }
}