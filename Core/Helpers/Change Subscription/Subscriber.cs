﻿// Copyright (c) 2015 ABXY Games, released under The MIT License
using UnityEngine;
using System.Collections;
using System.IO;
using System.Xml.Serialization;

namespace ABXY.OpenMVC.Helpers.ChangeSubscription
{
    
    /// <summary>
    /// Data container containing information for subscribers to model changes.
    /// </summary>
    public class Subscriber {


        /// <summary> The name of the variable this helper represents </summary>
        public string varName {
            get {
                return varName_P;
            }
        }
        /// <summary>Internal variable for varName property</summary>
        private string varName_P = "";


        /// <summary>
        /// This will be true once this class has been assigned to the first time. This way I can
        /// make sure I don't call subscribers on initial assignment
        /// </summary>
        private bool initialized = false;


        /// <summary>Assign to this variable every frame, and the object will be automatically checked for changes.
        /// DO NOT assign large objects. You have been warned! :-)</summary>
        public object WatchedObject {
            set {
                object newVersion = value;
                string serializedNewVersion = "";
                if (newVersion != null) {
                    serializedNewVersion = Subscriber.SerializeObject(newVersion);
                    latestVersion = newVersion;
                }
                if (serializedWatchedObject_P != serializedNewVersion) {
                    serializedWatchedObject_P = serializedNewVersion;
                    if (initialized)
                        UpdateSubscribers();
                    else
                        initialized = true;
                }
            }
        }


        /// <summary>String representation of the watched object. Used to make a true change check</summary>
        private string serializedWatchedObject_P;


        /// <summary>latest version of the watched object</summary>
        private object latestVersion;


        /// <summary>Contains the subscribers to the watched variable</summary>
        private ChangeSubscriber subscribers;


        /// <summary>
        /// Constructor!
        /// </summary>
        /// <param name="varName">The name of the variable</param>
        public Subscriber(string varName) {
            this.varName_P = varName;
        }


        /// <summary>
        /// Gets the number of subscribers watching for changes on this object
        /// </summary>
        public int subscriberCount {
            get {
                return subscribers.GetInvocationList().Length;
            }
        }


        /// <summary>
        /// Use this delegate definition to create your subscriber methods. This method will be 
        /// triggered when the watched variable changes
        /// </summary>
        /// <typeparam name="T">The expected type your subscriber expects back</typeparam>
        /// <param name="varName">The name of the variable that triggered the method</param>
        /// <param name="newObject">The updated object</param>
        public delegate void ChangeSubscriber(string varName, object newObject);


        /// <summary>
        /// Call this method to tell subscribers that the variable has been changed.
        /// </summary>
        private void UpdateSubscribers() {
            subscribers(varName_P, latestVersion);
        }


        /// <summary>
        /// Adds a subscriber to this class. The provided delegate will be called when the variable
        /// this class is watching changes
        /// </summary>
        /// <param name="sub">The delegate to call</param>
        public void AddSubscriber(ChangeSubscriber sub) {
            subscribers -= sub;// In case it's already added
            subscribers += sub;
        }


        /// <summary>
        /// Removes a subscriver from this class. The provided delegate will no longer be called by this class
        /// </summary>
        /// <param name="sub">The subscriber to remove</param>
        public void RemoveSubscriber(ChangeSubscriber sub) {
            subscribers -= sub;
        }


        /// <summary>
        /// Takes a class and serializes it to a string. Adapted from 
        /// http://stackoverflow.com/questions/2434534/serialize-an-object-to-string.
        /// Thanks DTB!
        /// </summary>
        /// <param name="toSerialize">Object to serialize</param>
        /// <returns>String representation of the provided object</returns>
        private static string SerializeObject(object toSerialize) {
            XmlSerializer xmlSerializer = new XmlSerializer(toSerialize.GetType());
            using (StringWriter textWriter = new StringWriter())
            {
                xmlSerializer.Serialize(textWriter, toSerialize);
                return textWriter.ToString();
            }
        }
    }
}