﻿using System.Reflection;
using ABXY.OpenMVC.Helpers.DelegateSystem;

namespace ABXY.OpenMVC.Helpers.Methods
{
    /// <summary>
    /// Data structure for holding a method and method context belonging 
    /// to a Model, View, or Controller
    /// </summary>
    public class Method
    {

        /// <summary>
        /// The method contained in this data structure
        /// </summary>
        public MethodInfo method
        {
            get;
            private set;
        }

        /// <summary>
        /// The context that "method" runs in
        /// </summary>
        public object targetObject
        {
            get;
            private set;
        }


        /// <summary>
        /// Was this method a member of a Model, View, and Controller?
        /// </summary>
        public DelegateDefinitions.MemberOf memberOf
        {
            get;
            private set;
        }


        /// <summary>
        /// Constructor for this object
        /// </summary>
        /// <param name="method">The method being stored in this object</param>
        /// <param name="targetObject">The context for that method</param>
        /// <param name="memberOf">Is this object's context a Model, View, or Controller?</param>
        public Method(MethodInfo method, object targetObject, DelegateDefinitions.MemberOf memberOf)
        {
            this.method = method;
            this.targetObject = targetObject;
            this.memberOf = memberOf;
        }
    }
}
