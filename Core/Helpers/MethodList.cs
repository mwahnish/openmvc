﻿using System.Collections.Generic;
using System.Reflection;
using ABXY.OpenMVC;
using ABXY.OpenMVC.Helpers.DelegateSystem;


namespace ABXY.OpenMVC.Helpers.Methods
{
    /// <summary>
    /// This data structure lists the methods available to the MVCMonobehaviour from its component
    /// Model, View, and Controller
    /// </summary>
    /// <typeparam name="modelType">The type of the target model. Must derive from Model</typeparam>
    /// <typeparam name="viewType">The type of the target view. Must derive from View</typeparam>
    /// <typeparam name="ControllerType">The type of the target controller. Must derive from Controller</typeparam>
    public class MethodList<modelType, viewType, ControllerType>
            where modelType : Model, new()
            where viewType : View<ControllerType, modelType>, new()
            where ControllerType : Controller<modelType>, new()
    {


        /// <summary>
        /// The list of methods available to the MVCMonobehaviour
        /// </summary>
        private Method[] methods = new Method[0];


        /// <summary>
        /// How many methods are in this list?
        /// </summary>
        public int numberOfMethods { get { return methods.Length; } }


        /// <summary>
        /// Constructor for a method list. Requires the user to provide a Model, View, and Controller. The methods in each 
        /// are automatically added to the list
        /// </summary>
        /// <param name="model"></param>
        /// <param name="view"></param>
        /// <param name="controller"></param>
        public MethodList(modelType model, viewType view, ControllerType controller)
        {

            // Adding each object to an array, just to make iteration easier
            object[] objects = new object[] { model, view, controller };

            // At the end of this method, this list will contain all the methods in the Model, View, and Controller
            List<Method> methodsOnly = new List<Method>();

            // Grabbing the members from each
            for (int index = 0; index < 3; index++)
            {
                // Getting all members
                MemberInfo[] members = GetMembers(objects[index]);

                // filtering out methods
                foreach (MemberInfo member in members)
                {
                    // Filtering out methods
                    if (member.MemberType == MemberTypes.Method)
                    {
                        // Checking if the method is defined as a delegate in the Model, View, and Controller
                        DelegateDefinitions.MemberOf memberOf = DelegateDefinitions.MemberOf.model;
                        switch (index)
                        {
                            case 0:
                                memberOf = DelegateDefinitions.MemberOf.model;
                                break;
                            case 1:
                                memberOf = DelegateDefinitions.MemberOf.view;
                                break;
                            case 2:
                                memberOf = DelegateDefinitions.MemberOf.control;
                                break;
                        }

                        // Adding method
                        methodsOnly.Add(new Method((MethodInfo)member, objects[index], memberOf));
                    }
                }
            }

            methods = methodsOnly.ToArray();
        }


        /// <summary>
        /// Gets all of the methods of the given type from the list. The return will be a list, and will never be null
        /// </summary>
        /// <param name="method">The method type to look for</param>
        /// <returns>A list of all of the methods matching the given type</returns>
        public Method[] GetMethod(DelegateDefinitions.events method)
        {

            // The list of methods to return
            List<Method> methodsToReturn = new List<Method>();

            // Checking for matches
            foreach (Method methodObject in methods)
            {
                if (methodObject.method.Name.ToLower() == method.ToString().ToLower())
                {
                    methodsToReturn.Add(methodObject);
                }
            }
            return methodsToReturn.ToArray();
        }


        /// <summary>
        /// Gets all members from the given object
        /// </summary>
        /// <param name="objectWithMembers">The object to search through</param>
        /// <returns>Returns an array of MemberInfo objects from the given class</returns>
        private MemberInfo[] GetMembers(object objectWithMembers)
        {
            MemberInfo[] allMembers = objectWithMembers.GetType().GetMembers(BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.FlattenHierarchy);
            return allMembers;
        }
    }
}