﻿using UnityEngine;


namespace ABXY.OpenMVC.Helpers.StateMachine {

    [System.Serializable]
    /// <summary>
    /// Container class for a state. Designed to be used in conjunction with the StateMachine Class
    /// </summary>
    public class State {

        [SerializeField]
        /// <summaryThe name of this state</summary>
        public string stateName = "New State";


        /// <summary>Delegate definition - called when this state is activated</summary>
        public delegate void OnActivateDelegate();


        /// <summary>
        /// Called when this state is activated.
        /// </summary>
        public OnActivateDelegate OnActivate;


        /// <summary>Delegate definition - called when this state is deactivated</summary>
        public delegate void OnDeactivateDelegate();
        /// <summary>
        /// Called when this state is deactivated
        /// </summary>
        public OnDeactivateDelegate OnDeactivate;


        /// <summary>Delegate definition - called every frame while this state is activated</summary>
        public delegate void WhileActivatedDelegate();

        /// <summary>Called every frame while this state is activated</summary>
        public WhileActivatedDelegate WhileActivated;


        [SerializeField]
        /// <summary>is activation of this state disabled?</summary>
        public bool enabled = true;


        /// <summary>
        /// Constructor
        /// </summary>
        public State() {
            enabled = true;
        }

        /// <summary>
        /// Constructor - allows the user to set the state name
        /// </summary>
        /// <param name="name">The name of the state</param>
        public State(string name)
        {
            enabled = true;
            stateName = name;
        }
    }

}