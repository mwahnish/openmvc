﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace ABXY.OpenMVC.Helpers.StateMachine {

    /// <summary>
    /// Simple state machine system - documentation to come
    /// </summary>
    [System.Serializable]
    public class StateMachine {

        /// <summary> List of states in this object</summary>
        public List<State> states = new List<State>(0);


        /// <summary>The current state of the state machine</summary>
        private State currentState_P;


        /// <summary>Returns the name of the current state of the state machine. Not assignable</summary>
        public string currentState {
            get {
                if (currentState_P == null && states.Count > 0)
                    currentState_P = states[0];
                return currentState_P.stateName;
            }
        }

        /// <summary>
        /// Constructor - Adds two states to the object as placeholders
        /// </summary>
        public StateMachine() {
            states.Add(new State());
            states.Add(new State());
        }

        /// <summary>
        /// Constructor - allows the user to add an initial set of states
        /// </summary>
        /// <param name="stateNames">The names of the states</param>
        public StateMachine(params string[] stateNames)
        {
            foreach (string stateName in stateNames)
            {
                states.Add(new State(stateName));
            }
        }

        /// <summary>
        /// Activates the given state name, assuming it is not disabled
        /// </summary>
        /// <param name="stateName"></param>
        public void ActivateState(string stateName) {
            State givenState = GetStateByString(stateName);
            if (givenState != null && givenState.enabled) {
                DeactivateCurrentState();
                ActivateState(givenState);
            }

        }


        /// <summary>
        /// Sets the given state as enabled or disabled. Disabiling a state prevents
        /// the state machine from switching to that state
        /// </summary>
        /// <param name="stateName">The name of the state to modify</param>
        /// <param name="enabled">Enable or disable the state</param>
        public void SetStateEnabled(string stateName, bool enabled) {
            State givenState = GetStateByString(stateName);
            if (givenState != null) {
                givenState.enabled = enabled;
            }
        }


        /// <summary>
        /// Searches through the state list for the given state name. Returns it
        /// if found, otherwise returns null
        /// </summary>
        /// <param name="stateName">The name of the state to search for</param>
        /// <returns>The state if found, or null</returns>
        private State GetStateByString(string stateName) {
            State returnedState = null;
            foreach (State state in states) {
                if (state.stateName.Equals(stateName)) {
                    returnedState = state;
                }
            }
            if (returnedState == null) {
                Debug.LogWarning("Warning, " + stateName + " is not a valid state");
            }
            return returnedState;
        }


        /// <summary>
        /// Adds an onactivate handler to the given state
        /// </summary>
        /// <param name="onActivate">The delegate to call when the state is activated</param>
        /// <param name="stateName">The name of the state</param>
        public void AddOnActivateHandler(State.OnActivateDelegate onActivate, string stateName) {
            State state = GetStateByString(stateName);
            if (state != null) {
                state.OnActivate -= onActivate;
                state.OnActivate += onActivate;
            }
        }


        /// <summary>
        /// Adds an onDeactivate handler to the given state
        /// </summary>
        /// <param name="onDeactivate">The delegate to call when the state is deactivated</param>
        /// <param name="stateName">The name of the state</param>
        public void AddOnDeactivateHandler(State.OnDeactivateDelegate onDeactivate, string stateName) {
            State state = GetStateByString(stateName);
            if (state != null) {
                state.OnDeactivate -= onDeactivate;
                state.OnDeactivate += onDeactivate;
            }
        }


        /// <summary>
        /// Adds a whileActivated handler to the given state
        /// </summary>
        /// <param name="whileActivate">The delegate to call while the state is activated</param>
        /// <param name="stateName">The name of the state</param>
        public void AddWhileActivatedHandler(State.WhileActivatedDelegate whileActivate, string stateName) {
            State state = GetStateByString(stateName);
            if (state != null) {
                state.WhileActivated -= whileActivate;
                state.WhileActivated += whileActivate;
            }
        }


        /// <summary>
        /// Activates a state, sets it as the new current. Calls proper activation functions
        /// </summary>
        /// <param name="newCurrentState">The new current state</param>
        private void ActivateState(State newCurrentState) {
            if (newCurrentState.OnActivate != null)
                newCurrentState.OnActivate();
            currentState_P = newCurrentState;
        }


        /// <summary>
        /// Deactivates the current state, calls the appropriate deactivation functions
        /// </summary>
        private void DeactivateCurrentState() {
            if (currentState_P != null) {
                if (currentState_P.OnDeactivate != null)
                    currentState_P.OnDeactivate();
                currentState_P = null;
            }
        }


        /// <summary>
        /// Updates the currently activated state. Must be called every time you want to update a state
        /// i.e., every frame
        /// </summary>
        public void Update() {
            if (currentState_P != null)
                currentState_P.WhileActivated();
        }
    }
}