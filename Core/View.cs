﻿// Copyright (c) 2015 ABXY Games, released under The MIT License
using UnityEngine;
using System.Collections;
using ABXY.OpenMVC.Helpers;

namespace ABXY.OpenMVC {

    /// <summary>
    /// Extend this class to create a view. The View class is responsible for any logic related to
    /// the presentation of the MVCMonobehaviour. It only stores data in the model, and it only 
    /// manipulates data through the controller. The View is allowed access to the model and controller.
    /// dditionally, a View can only exist alongside a Controller, a Model, and the MVCMonobehaviour container. 
    /// 
    /// Note: The Unity callbacks in this class operate the same as the do in the standard Monobehaviour. As a result, 
    /// many method descriptions are pulled straight from Unity's documentation.
    /// </summary>
    /// <typeparam name="controllerType">The type of the related controller</typeparam>
    /// <typeparam name="modelType">The type of the related model</typeparam>
    [System.Serializable]
    public class View <controllerType,modelType>
        where controllerType : Controller<modelType>
        where modelType : Model{

        /// <summary>The MVCMonobehaviour that owns this view</summary>
        private GenericMVCMonobehaviour owner;

        /// <summary>
        /// The cached model related to this view
        /// </summary>
        private modelType model_P;
        
        /// <summary>
        /// The cached controller related to this view
        /// </summary>
        private controllerType controller_P;

        /// <summary>
        /// Sets up interoperability between the view class, its parent monobehavior, and it's related
        /// controller and model. You'll never need to call this manually
        /// </summary>
        /// <param name="owner">The parent Monobehavior</param>
        /// <param name="model">The view's related model</param>
        /// <param name="controller">The view's related controller</param>
        public void initialize(GenericMVCMonobehaviour owner, modelType model, controllerType controller) {
            this.owner = owner;
            model_P = model;
            controller_P = controller;
        }

        /// <summary>Access point for the this MVCMonobehaviour's model</summary>
        protected modelType model {
            get {
                return model_P;
            }
        }

        /// <summary>Access point for the this MVCMonobehaviour's controller</summary>
        protected controllerType controller {
            get {
                return controller_P;
            }
        }

        /// <summary>
        /// Disabling this lets you skip the GUI layout phase.
        /// </summary>
        public bool useGUILayout {
            get {
                return owner.useGUILayout;
            }
            set {
                owner.useGUILayout = value;
            }
        }

        //Monobehaviour methods
        /*
        public override string ToString() {
            return owner.ToString();
        }*/


        /// <summary>
        /// Caches the owning MVCMonobehaviour for internal use. This is called automatically, no need to call manually
        /// </summary>
        /// <param name="owner">Parent MVCMonobehaviour</param>
        public void SetOwner(GenericMVCMonobehaviour owner) {
            this.owner = owner;
            if (owner == null){
                Debug.LogError("Context must not be null");
            }
        }

        #if (DOCUMENTATION)
        /// <summary>
        /// Start is called on the frame when a script is enabled just before any of the Update methods is called the first time.
        /// </summary>
        /// <example>
        /// <code language="csharp">
        /// <![CDATA[
        /// using UnityEngine;
        /// using System.Collections;
        /// using ABXY.OpenMVC;
        ///
        /// public class ExampleGameobject :  MVCMonobehaviour<ExampleModel,ExampleView,ExampleController>{}
        /// 
        /// public class ExampleModel: Model {
        /// 
        ///     /* 
        ///      * Getter and private caching for the Gameobject's Renderer. Renderer 
        ///      * can only be accessed through model
        ///      */
        ///     private Renderer renderer_P;
        ///     public Renderer renderer{
        ///         get{
        ///                 if (renderer_p == null)
        ///                     renderer_p = (Renderer)GetComponent(typeof(Renderer));
        ///                 return Renderer_p;
        ///            }
        ///     }
        /// 
        /// }
        /// 
        /// public class ExampleView : View<ExampleController,ExampleModel>{
        ///     
        ///     public void Start(){
        ///         model.renderer.enabled = false;
        ///         
        ///     }
        /// }
        /// 
        /// public class ExampleController : Controller<ExampleModel>{}
        /// ]]>
        /// </code>
        /// </example> 
        public void Start() {

        }

        /// <summary>
        /// Reset to default values.
        /// </summary>
        /// <example>
        /// <code language="csharp">
        /// <![CDATA[
        /// using UnityEngine;
        /// using System.Collections;
        /// using ABXY.OpenMVC;
        ///
        /// public class ExampleGameobject :  MVCMonobehaviour<ExampleModel,ExampleView,ExampleController>{}
        /// 
        /// public class ExampleModel: Model {}
        /// 
        /// public class ExampleView : View<ExampleController,ExampleModel>{
        ///     public void Reset(){
        ///         Debug.Log("This object has been reset");
        ///     }
        /// }
        /// 
        /// public class ExampleController : Controller<ExampleModel>{}
        /// ]]>
        /// </code>
        /// </example>  
        public void Reset() {

        }

        /// <summary>
        /// OnWillRenderObject is called once for each camera if the object is visible.
        /// </summary>
        /// <example>
        /// <code language="csharp">
        /// <![CDATA[
        /// using UnityEngine;
        /// using System.Collections;
        /// using ABXY.OpenMVC;
        ///
        /// public class ExampleGameobject :  MVCMonobehaviour<ExampleModel,ExampleView,ExampleController>{}
        /// 
        /// public class ExampleModel: Model {}
        /// 
        /// public class ExampleView : View<ExampleController,ExampleModel>{
        ///     public void OnWillRenderObject(){
        ///         Debug.Log("This object is visible and will be rendered by a camera");
        ///     }
        /// }
        /// 
        /// public class ExampleController : Controller<ExampleModel>{}
        /// ]]>
        /// </code>
        /// </example>  
        public void OnWillRenderObject() {

        }

        /// <summary>
        /// This function is called when the script is loaded or a value is changed in the inspector (Called in the editor only).
        /// </summary>
        /// <example>
        /// <code language="csharp">
        /// <![CDATA[
        /// using UnityEngine;
        /// using System.Collections;
        /// using ABXY.OpenMVC;
        ///
        /// public class ExampleGameobject :  MVCMonobehaviour<ExampleModel,ExampleView,ExampleController>{}
        /// 
        /// public class ExampleModel: Model {}
        /// 
        /// public class ExampleView : View<ExampleController,ExampleModel>{
        ///     public void OnValidate(){
        ///         Debug.Log("This object has been deserialized");
        ///     }
        /// }
        /// 
        /// public class ExampleController : Controller<ExampleModel>{}
        /// ]]>
        /// </code>
        /// </example> 
        public void OnValidate() {

        }

        /// <summary>
        /// OnGUI is called for rendering and handling GUI events.
        /// </summary>
        /// <example>
        /// <code language="csharp">
        /// <![CDATA[
        /// using UnityEngine;
        /// using System.Collections;
        /// using ABXY.OpenMVC;
        ///
        /// public class ExampleGameobject :  MVCMonobehaviour<ExampleModel,ExampleView,ExampleController>{}
        /// 
        /// public class ExampleModel: Model {}
        /// 
        /// public class ExampleView : View<ExampleController,ExampleModel>{
        ///     public void OnGUI(){
        ///         if (GUILayout.Button("I'm a button!")) {
        ///         Debug.Log("Button 1 clicked!");
        ///    }
        ///    if (GUILayout.Button("I'm also a button!")) {
        ///        Debug.Log("Button 2 clicked!");
        ///    }
        /// }
        /// 
        /// public class ExampleController : Controller<ExampleModel>{}
        /// ]]>
        /// </code>
        /// </example> 
        public void OnGUI() {
            
        }

        /// <summary>
        /// This function is called when the object becomes enabled and active.
        /// </summary>
        /// <example>
        /// <code language="csharp">
        /// <![CDATA[
        /// using UnityEngine;
        /// using System.Collections;
        /// using ABXY.OpenMVC;
        ///
        /// public class ExampleGameobject :  MVCMonobehaviour<ExampleModel,ExampleView,ExampleController>{}
        /// 
        /// public class ExampleModel: Model {
        /// }
        /// 
        /// public class ExampleView : View<ExampleController,ExampleModel>{
        ///     public void OnEnabled(){
        ///         Debug.Log("This object Has been enabled");
        ///     }
        /// }
        /// 
        /// public class ExampleController : Controller<ExampleModel>{}
        /// ]]>
        /// </code>
        /// </example> 
        public void OnEnable() {

        }

        /// <summary>
        /// Implement this OnDrawGizmosSelected if you want to draw gizmos only if the object is selected.
        /// </summary>
        /// <example>
        /// <code language="csharp">
        /// <![CDATA[
        /// using UnityEngine;
        /// using System.Collections;
        /// using ABXY.OpenMVC;
        ///
        /// public class ExampleGameobject :  MVCMonobehaviour<ExampleModel,ExampleView,ExampleController>{}
        /// 
        /// public class ExampleModel: Model {
        ///     public Vector3 pointInSpace = new Vector3(7f,42f,13f);
        /// 
        /// }
        /// 
        /// public class ExampleView : View<ExampleController,ExampleModel>{
        /// 
        ///     // Going to draw a sphere at the location specified in the model
        ///     public void OnDrawGizmosSelected(){
        ///         Gizmos.DrawSphere( model.pointInSpace, 1f);
        ///     }
        /// }
        /// 
        /// public class ExampleController : Controller<ExampleModel>{}
        /// ]]>
        /// </code>
        /// </example> 

        public void OnDrawGizmosSelected() {
            
        }

        /// <summary>
        /// Implement OnDrawGizmos if you want to draw gizmos that are also pickable and always drawn.
        /// </summary>
        /// <example>
        /// <code language="csharp">
        /// <![CDATA[
        /// using UnityEngine;
        /// using System.Collections;
        /// using ABXY.OpenMVC;
        ///
        /// public class ExampleGameobject :  MVCMonobehaviour<ExampleModel,ExampleView,ExampleController>{}
        /// 
        /// public class ExampleModel: Model {
        ///     public Vector3 pointInSpace = new Vector3(7f,42f,13f);
        /// 
        /// }
        /// 
        /// public class ExampleView : View<ExampleController,ExampleModel>{
        /// 
        ///     // Going to draw a sphere at the location specified in the model
        ///     public void OnDrawGizmos(){
        ///         Gizmos.DrawSphere( model.pointInSpace, 1f);
        ///     }
        /// }
        /// 
        /// public class ExampleController : Controller<ExampleModel>{}
        /// ]]>
        /// </code>
        /// </example> 
        public void OnDrawGizmos() {

        }
        #endif

    }
}
