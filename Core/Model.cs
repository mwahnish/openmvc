﻿// Copyright (c) 2015 ABXY Games, released under The MIT License
using UnityEngine;
using System.Collections.Generic;
using ABXY.OpenMVC.Helpers;
using ABXY.OpenMVC.Helpers.ChangeSubscription;
using VexeEmbedded.Runtime.Types;

namespace ABXY.OpenMVC {

    /// <summary>
    /// Extend this class to create a model. The model class contains the data for an MVCMonobehaviour. It performs no logic on it's own,
    /// only including Unity callback functions related to setting up the object (Start, OnValidate, etc.). 
    /// The model is not allowed to access members of Controller or View classes. Additionally, a model
    /// can only exist alongside a Controller, a View, and the MVCMonobehaviour container. 
    /// 
    /// Note: The Unity callbacks in this class operate the same as the do in the standard Monobehaviour. As a result, 
    /// many method descriptions are pulled straight from Unity's documentation.
    /// </summary>
    [System.Serializable]
    public class Model{

        /// <summary>The MVCMonobehaviour that owns this model</summary>
        private GenericMVCMonobehaviour owner;

        [HideAttribute]
        public ChangeSubscriptionHandler changeSubscription = new ChangeSubscriptionHandler();

        /// <summary>
        /// Sets up interoperability between the model class and the parent monobehavior. 
        /// You'll never need to call this manually
        /// </summary>
        /// <param name="owner">The parent Monobehavior</param>
        public void Initialize(GenericMVCMonobehaviour owner) {
            this.owner = owner;

        }

        //Methods from Monobehaviour
        //TODO: Stub 0 BroadcastMessage()
        
        /// <summary>
        /// Is the game object tagged with tag? - From Unity3d.com documentation
        /// </summary>
        /// <param name="tag">The tag to check</param>
        /// <returns>Returns true if the object is tagged with tag</returns>
        public bool CompareTag(string tag) {
            return owner.CompareTag(tag);
        }
        
        /// <summary>
        /// Returns a component of the given type if one is attached to this gameobject. Returns null if none exists. Note that
        /// calls to this function are expensive. It is recommended to call once and cache the results.
        /// </summary>
        /// <param name="type">The type of the component to search for</param>
        /// <returns>The retrieved component</returns>
        /// <example>
        /// <code language="csharp">
        /// <![CDATA[
        /// using UnityEngine;
        /// using System.Collections;
        /// using ABXY.OpenMVC;
        ///
        /// public class ExampleGameobject :  MVCMonobehaviour<ExampleModel,ExampleView,ExampleController>{}
        /// 
        /// public class ExampleModel: Model {
        ///     
        ///     public Rigidbody rigidbody;
        ///     
        ///     public void Start(){
        ///         rigidbody = (Rigidbody)GetComponent(typeof(Rigidbody));
        ///     }
        /// }
        /// 
        /// public class ExampleView : View<ExampleController,ExampleModel>{}
        /// 
        /// public class ExampleController : Controller<ExampleModel>{}
        /// ]]>
        /// </code>
        /// </example>
        public Component GetComponent(System.Type type) {
            return owner.GetComponent(type);
        }
        
        /// <summary>
        /// Returns a component of the given type if one is attached to this gameobject. Returns null if none exists. Note that
        /// calls to this function are expensive. It is recommended to call once and cache the results.
        /// </summary>
        /// <param name="type">String representing the tyoe to search for</param>
        /// <returns>The retrieved component</returns>
        /// /// <example>
        /// <code language="csharp">
        /// <![CDATA[
        /// using UnityEngine;
        /// using System.Collections;
        /// using ABXY.OpenMVC;
        ///
        /// public class ExampleGameobject :  MVCMonobehaviour<ExampleModel,ExampleView,ExampleController>{}
        /// 
        /// public class ExampleModel: Model {
        ///     
        ///     public Rigidbody rigidbody;
        ///     
        ///     public void Start(){
        ///         rigidbody = (Rigidbody)GetComponent("Rigidbody");
        ///     }
        /// }
        /// 
        /// public class ExampleView : View<ExampleController,ExampleModel>{}
        /// 
        /// public class ExampleController : Controller<ExampleModel>{}
        /// ]]>
        /// </code>
        /// </example>
        public Component GetComponent(string type) {
            return owner.GetComponent(type);
        }
       
        /// <summary>
        /// Returns a component of the given type if one is attached to a child of this gameobject. Returns null if none 
        /// exists. Note that calls to this function are expensive. It is recommended to call once and cache the results.
        /// </summary>
        /// <param name="t">The type of the component of search for</param>
        /// <returns>The retrieved component</returns>
        public Component GetComponentInChildren(System.Type t) {
            return owner.GetComponentInChildren(t);
        }
        
       //TODO Stub GetComponentInChildren<t>()
        
        /// <summary>
        /// Returns an array of components of the given type if one or more are attached to this gameobject. Returns null if none exists. Note that
        /// calls to this function are expensive. It is recommended to call once and cache the results.
        /// </summary>
        /// <param name="type">The type of the components to search for</param>
        /// <returns>An array of the retrieved components</returns>
        public Component[] GetComponents(System.Type type) {
            return owner.GetComponents(type);
        }

        /// <summary>
        /// Utility method. If you have variables that are required to not be null, call this method in Setup() with those
        /// variables as arguments. This method will log an error if any are null.
        /// </summary>
        /// <param name="requiredObjects">A list of objects that are required to be non-null at start</param>
       public void RequiredVariableCheck(params Object[] requiredObjects) {
           for (int index = 0; index < requiredObjects.Length; index++) {
               if (requiredObjects[index] == null) {
                   Debug.LogError("Error! Required object #" + requiredObjects[index].ToString() + " is NULL");
               }
           }
       }
        
        /// <summary>
        /// Caches the owning MVCMonobehaviour for internal use. This is called automatically, no need to call manually
        /// </summary>
        /// <param name="owner">Parent MVCMonobehaviour</param>
       public void SetOwner(GenericMVCMonobehaviour owner) {
            this.owner = owner;
            if (owner == null) {
                Debug.LogError("Context must not be null");
            }
        }

       /// <summary>
       /// Override this method to return the variables you wish to make subscribeable. Other classes
       /// can then subscribe to be notified when those variables change. Caching the dictionary you
       /// are returning is recommended, this method will be called often
       /// </summary>
       /// <returns>Dictionary of subscribeable variables, where the key is a string that is the name
       /// of the variable, and the value is the value of the variable</returns>
       /// <example>
       /// A quick and dirty example
       /// <code language="csharp">
       /// <![CDATA[
       /// using UnityEngine;
       /// using System.Collections;
       /// using ABXY.OpenMVC;
       ///
       /// public class ExampleGameobject :  MVCMonobehaviour<ExampleModel,ExampleView,ExampleController>{}
       /// 
       /// public class ExampleModel: Model {
       ///     
       ///     public float someNumber = 42f;
       ///     
       ///     public override Dictionary<string, object> IdentifySubscribableVariables(){
       ///     
       ///          Dictionary<string, object> variableDictionary = new Dictionary<string, object>();
       ///          variableDictionary.Add("someNumber", someNumber);
       ///          return variableDictionary;
       ///     }
       /// }
       /// 
       /// public class ExampleView : View<ExampleController,ExampleModel>{}
       /// 
       /// public class ExampleController : Controller<ExampleModel>{}
       /// ]]>
       /// </code>
       /// 
       /// A more efficient implementation
       /// <code language="csharp">
       /// <![CDATA[
       /// using UnityEngine;
       /// using System.Collections;
       /// using ABXY.OpenMVC;
       ///
       /// public class ExampleGameobject :  MVCMonobehaviour<ExampleModel,ExampleView,ExampleController>{}
       /// 
       /// public class ExampleModel: Model {
       ///     
       ///     public float someNumber = 42f;
       /// 
       ///     Dictionary<string, object> variableDictionary;
       /// 
       ///     public void Start(){
       ///          variableDictionary = new Dictionary<string, object>();
       ///          variableDictionary.Add("someNumber", someNumber);
       ///     }
       /// 
       ///     public override Dictionary<string, object> IdentifySubscribableVariables(){
       ///          variableDictionary["someNumber"] = someNumber;
       ///          return variableDictionary;
       ///     }
       /// }
       /// 
       /// public class ExampleView : View<ExampleController,ExampleModel>{}
       /// 
       /// public class ExampleController : Controller<ExampleModel>{}
       /// ]]>
       /// </code>
       /// </example>  
       protected virtual Dictionary<string, object> IdentifySubscribeableVariables() {
           return new Dictionary<string, object>();
       }

       /// <summary>
       /// Updates other classes that have registered for variable changes. In practice, you will never
       /// need to call this, it is called autmatically every frame.
       /// </summary>
       public void RefreshSubscribers() {
           changeSubscription.RefreshSubscribers(IdentifySubscribeableVariables());
       }

        #if (DOCUMENTATION)
        /// <summary>
        /// Start is called on the frame when a script is enabled just before any of the Update methods is called the first time.
        /// </summary>
        /// <example>
        /// <code language="csharp">
        /// <![CDATA[
        /// using UnityEngine;
        /// using System.Collections;
        /// using ABXY.OpenMVC;
        ///
        /// public class ExampleGameobject :  MVCMonobehaviour<ExampleModel,ExampleView,ExampleController>{}
        /// 
        /// public class ExampleModel: Model {
        ///     
        ///     public Rigidbody rigidbody;
        ///     
        ///     public void Start(){
       ///         rigidbody = (Rigidbody)GetComponent(typeof(Rigidbody));
        ///     }
        /// }
        /// 
        /// public class ExampleView : View<ExampleController,ExampleModel>{}
        /// 
        /// public class ExampleController : Controller<ExampleModel>{}
        /// ]]>
        /// </code>
        /// </example>  
        public void Start() {

        }

        /// <summary>
        /// Reset to default values.
        /// </summary>
        /// <example>
        /// <code language="csharp">
        /// <![CDATA[
        /// using UnityEngine;
        /// using System.Collections;
        /// using ABXY.OpenMVC;
        ///
        /// public class ExampleGameobject :  MVCMonobehaviour<ExampleModel,ExampleView,ExampleController>{}
        /// 
        /// public class ExampleModel: Model {
        ///     public void Reset(){
        ///         Debug.Log("This object has been reset");
        ///     }
        /// }
        /// 
        /// public class ExampleView : View<ExampleController,ExampleModel>{}
        /// 
        /// public class ExampleController : Controller<ExampleModel>{}
        /// ]]>
        /// </code>
        /// </example>  
        public void Reset() {

        }

        /// <summary>
        /// This function is called when the script is loaded or a value is changed in the inspector (Called in the editor only).
        /// </summary>
        /// /// <example>
        /// <code language="csharp">
        /// <![CDATA[
        /// using UnityEngine;
        /// using System.Collections;
        /// using ABXY.OpenMVC;
        ///
        /// public class ExampleGameobject :  MVCMonobehaviour<ExampleModel,ExampleView,ExampleController>{}
        /// 
        /// public class ExampleModel: Model {
        ///     public void OnValidate(){
        ///         Debug.Log("This object has been deserialized");
        ///     }
        /// }
        /// 
        /// public class ExampleView : View<ExampleController,ExampleModel>{}
        /// 
        /// public class ExampleController : Controller<ExampleModel>{}
        /// ]]>
        /// </code>
        /// </example>  
        public void OnValidate() {

        }

        /// <summary>
        /// This function is called when the object becomes enabled and active.
        /// </summary>
        /// <example>
        /// <code language="csharp">
        /// <![CDATA[
        /// using UnityEngine;
        /// using System.Collections;
        /// using ABXY.OpenMVC;
        ///
        /// public class ExampleGameobject :  MVCMonobehaviour<ExampleModel,ExampleView,ExampleController>{}
        /// 
        /// public class ExampleModel: Model {
        ///     public void OnEnabled(){
        ///         Debug.Log("This object Has been enabled");
        ///     }
        /// }
        /// 
        /// public class ExampleView : View<ExampleController,ExampleModel>{}
        /// 
        /// public class ExampleController : Controller<ExampleModel>{}
        /// ]]>
        /// </code>
        /// </example>  
        public void OnEnable() {

        }

        /// <summary>
        /// This function is called when the behaviour becomes disabled () or inactive.
        /// </summary>
        /// <example>
        /// <code language="csharp">
        /// <![CDATA[
        /// using UnityEngine;
        /// using System.Collections;
        /// using ABXY.OpenMVC;
        ///
        /// public class ExampleGameobject :  MVCMonobehaviour<ExampleModel,ExampleView,ExampleController>{}
        /// 
        /// public class ExampleModel: Model {
        ///     public void OnDisable(){
        ///         Debug.Log("This object has been disabled");
        ///     }
        /// }
        /// 
        /// public class ExampleView : View<ExampleController,ExampleModel>{}
        /// 
        /// public class ExampleController : Controller<ExampleModel>{}
        /// ]]>
        /// </code>
        /// </example>  
        public void OnDisable() {

        }

        /// <summary>
        /// This function is called when the MVCMonoBehaviour will be destroyed.
        /// </summary>
        /// <example>
        /// <code language="csharp">
        /// <![CDATA[
        /// using UnityEngine;
        /// using System.Collections;
        /// using ABXY.OpenMVC;
        ///
        /// public class ExampleGameobject :  MVCMonobehaviour<ExampleModel,ExampleView,ExampleController>{}
        /// 
        /// public class ExampleModel: Model {
        ///     public void OnDestroy(){
        ///         Debug.Log("This object is being destroyed");
        ///     }
        /// }
        /// 
        /// public class ExampleView : View<ExampleController,ExampleModel>{}
        /// 
        /// public class ExampleController : Controller<ExampleModel>{}
        /// ]]>
        /// </code>
        /// </example>  
        public void OnDestroy() {

        }
        #endif

    }
}
