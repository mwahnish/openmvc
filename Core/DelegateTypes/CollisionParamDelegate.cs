﻿// Copyright (c) 2016 ABXY Games, released under The MIT License
using UnityEngine;
using ABXY.OpenMVC.Helpers.DelegateSystem;
using ABXY.OpenMVC.Helpers.Methods;

namespace ABXY.OpenMVC.Helpers.DelegateTypes
{

    /// <summary>
    /// A generic Delegate for calling monobehaviour methods requiring booleans
    /// </summary>
    public class CollisionParamDelegate
    {

        /// <summary>
        /// The method definition for this delegate
        /// </summary>
        /// <param name="collision">The boolean parameter for the Collision</param>
        private delegate void methodDef(Collision collision);

        /// <summary>
        /// The set of delegates being called by this handler
        /// </summary>
        private methodDef method;


        /// <summary>
        /// Assigns the given methods to this delegate
        /// </summary>
        /// <param name="incomingMethods">An array of methods to be called by this delegate</param>
        public void Assign(Method[] incomingMethods)
        {
            foreach (Method incomingMethod in incomingMethods)
            {
                //Checking if assignment should be allowed
                bool allowAssignment = false;

                if (incomingMethod.memberOf == DelegateDefinitions.MemberOf.model)
                {
                    allowAssignment =
                        (DelegateDefinitions.IsADelegateOf(DelegateDefinitions.MemberOf.model, incomingMethod.method.Name)) ?
                        true : allowAssignment;
                }
                else if (incomingMethod.memberOf == DelegateDefinitions.MemberOf.view)
                {
                    allowAssignment =
                        (DelegateDefinitions.IsADelegateOf(DelegateDefinitions.MemberOf.view, incomingMethod.method.Name)) ?
                        true : allowAssignment;
                }
                else if (incomingMethod.memberOf == DelegateDefinitions.MemberOf.control)
                {
                    allowAssignment =
                        (DelegateDefinitions.IsADelegateOf(DelegateDefinitions.MemberOf.control, incomingMethod.method.Name)) ?
                        true : allowAssignment;
                }

                // Doing assignment
                if (allowAssignment)
                    method += (methodDef)System.Delegate.CreateDelegate(typeof(methodDef), incomingMethod.targetObject, incomingMethod.method);
            }
        }

        /// <summary>
        /// Checks if this delgate has been assigned to.
        /// </summary>
        public bool Assigned { get { return method != null; } }

        /// <summary>
        /// Executes this delegate
        /// </summary>
        /// <param name="collision">The Collision parameter for the delegate</param>
        public void Execute(Collision collision)
        {
            method(collision);
        }
    }
}